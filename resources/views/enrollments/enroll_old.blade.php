@extends('layouts.vendor')

@section('title', 'Enrollment Form')

@section('style')

@endsection

@section('content')

<div class="header-hero"></div>

<div class="container register">
	
	<div class="row">
		<div class="col-md-3 register-left">
			<!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/> -->
			<h3>Welcome</h3>
			<p>This is JPI Technologies Enrollment Form. Please see the instruction</p>
			<a href="/instructions">
				<input type="submit" name="" value="Instructions"/><br/>
			</a>
		</div>
		<div class="col-md-9 register-right">
			<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
<!-- 				<li class="nav-item">
					<a class="nav-link active" id="instruction-tab" data-toggle="tab" href="#instruction" role="tab" aria-controls="instruction" aria-selected="true">Instruction</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="enrollment-form-tab" data-toggle="tab" href="#enrollment-form" role="tab" aria-controls="enrollment-form" aria-selected="false">Form</a>
				</li> -->
			</ul>
        	<form action="{{ route('enrollment.update_old') }}" method="POST" enctype="multipart/form-data">

            @csrf
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="enrollment-form" role="tabpanel" aria-labelledby="enrollment-form-tab">

						<div class="align-alert">
							@include('flash::message')
							@include('layouts.message')
						</div>
						<h3 class="register-heading">Enrollment Form</h3>

						<div class="row register-form">

							<!-- Column One -->
							<div class="col-md-6">

								<div class="form-group">
									{{ Form::number('lrn_number', '', ['class' => 'form-control', 'placeholder' => 'LRN Number']) }}
								</div>

								<div class="form-group">
									<select class="form-control selection" name="year_level" id="year_level">
										<option value="" class="hidden" selected disabled> Please select Year Level</option>
										<option value="year_level_11" {{ (old('year_level') == 'year_level_11') ? 'selected' : '' }} >Year Level 11</option>
										<option value="year_level_12" {{ (old('year_level') == 'year_level_12') ? 'selected' : '' }} >Year Level 12</option>
									</select>
								</div>

							</div>

							<div class="col-md-6">

								<div class="form-group">
									<select class="form-control selection" name="semester" id="semester">
										<option value="" class="hidden" selected disabled> Please select Semester</option>
										<option value="1st_semester" {{ (old('semester') == '1st_semester') ? 'selected' : '' }} >1st Semester</option>
										<option value="2nd_semester" {{ (old('semester') == '2nd_semester') ? 'selected' : '' }} >2nd Semester</option>
									</select>
								</div>

								<input type="submit" class="btnRegister mt-0"  value="Submit"/>
							</div>

						</div>
					</div>	
				</div>
			</form>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="subjects" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h6 class="modal-title" id="exampleModalLabel">Subjects</h6>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">

      		<!-- Grade 11 -->
      		<!-- FIRST SEMESTER -->

	      	<div id="year_level_11" class="hidden">

		      	<div id="1st_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

						  	<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Business Mathematics</li>
							<li class="list-group-item">Organization and Management</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

						  	<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>

						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Pre-Calculus</li>
							<li class="list-group-item">General Biology 1</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>


						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disciplines and Ideas in the Social Sciences</li>
						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Organization and Management</li>
							<li class="list-group-item">Elective 1</li>
						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>

	      		<!-- Grade 11 -->
	      		<!-- Second SEMESTER -->

		      	<div id="2nd_semester" class="semester-target"> 

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Fundamentals of Accountancy, Business and Management 1</li>
							<li class="list-group-item">Principles of Marketing</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Basic Calculus</li>
							<li class="list-group-item">General Biology 2</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disciplines and Ideas in the Applied Social Sciences</li>
							<li class="list-group-item">Creative Writing/Malikhaing Pagsulat</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disaster Readiness and Risk Reduction</li>
							<li class="list-group-item">HUMSS 1:Creative Writing/Malikhaing Pagsulat</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>
							<!-- <li class="list-group-item">Computer System Servicing (NC II)</li> -->

						</ul>

			      	</div>
		      	</div>

	      	</div>

      		<!-- Grade 12 -->
      		<!-- First SEMESTER -->

	      	<div id="year_level_12" class="hidden">

		      	<div id="1st_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Fundamentals of Accountancy, Business and Management 2</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">General Physics 1</li>
							<li class="list-group-item">General Chemistry 1</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<!-- <li class="list-group-item">21st Century Literature from the Philippines and the World</li> -->
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Introduction to World Religions and Belief Systems</li>
							<li class="list-group-item">Philippine Politics and Governance</li>
							<li class="list-group-item">Creative Nonfiction</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<!-- <li class="list-group-item">21st Century Literature from the Philippines and the World</li> -->
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">HUMSS 2: Introduction to World Religions and Belief Systems</li>
							<li class="list-group-item">Philippine Politics and Governance</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>

	      		<!-- Grade 12 -->
	      		<!-- Second SEMESTER -->

		      	<div id="2nd_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Applied Economics</li>
							<li class="list-group-item">Business Ethics and Social Responsibility</li>
							<li class="list-group-item">Business Finance</li>
							<li class="list-group-item">Business Enterprise Simulation</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">General Physics 2</li>
							<li class="list-group-item">General Chemistry 2</li>
							<li class="list-group-item">Research/Capstone Project</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Trends, Networks, and Critical Thinking in the 21st Century</li>
							<li class="list-group-item">Community Engagement, Solidarity, and Citizenship (CSC)</li>
							<li class="list-group-item">Culminating Activity</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Applied Economics</li>
							<li class="list-group-item">Elective 2 (from any Track/Strand)</li>
							<li class="list-group-item">Work Immersion/Culminating Activity</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>
							<li class="list-group-item">Work Immersion</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>
		      	
	      	</div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

</div>

@endsection


@section('script')

@endsection