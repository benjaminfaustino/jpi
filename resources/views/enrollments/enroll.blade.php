@extends('layouts.vendor')

@section('title', 'Enrollment Form')

@section('style')

@endsection

@section('content')

<div class="header-hero"></div>

<div class="container register">
	
	<div class="row">
		<div class="col-md-3 register-left">
			<!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/> -->
			<h3>Welcome</h3>
			<p>This is JPI Technologies Enrollment Form. Please see the instruction</p>
			<a href="/instructions">
				<input type="submit" name="" value="Instructions"/><br/>
			</a>
		</div>
		<div class="col-md-9 register-right">
			<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
<!-- 				<li class="nav-item">
					<a class="nav-link active" id="instruction-tab" data-toggle="tab" href="#instruction" role="tab" aria-controls="instruction" aria-selected="true">Instruction</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="enrollment-form-tab" data-toggle="tab" href="#enrollment-form" role="tab" aria-controls="enrollment-form" aria-selected="false">Form</a>
				</li> -->
			</ul>
        	<form action="{{ route('enrollment.store') }}" method="POST" enctype="multipart/form-data">

            @csrf
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="enrollment-form" role="tabpanel" aria-labelledby="enrollment-form-tab">

						<div class="align-alert">
							@include('flash::message')
							@include('layouts.message')
						</div>
						<h3 class="register-heading">Enrollment Form For New Student</h3>
						<h6 class="register-heading">
							<a href="/enrollment-form-old">Click Here For Old/Returning Students</a>
						</h6>

						<div class="row register-form">

							<!-- Column One -->
							<div class="col-md-6">
		<!-- 							<div class="form-group">
									<div class="maxl">
										<label class="radio inline"> 
											<input type="radio" name="enrollment_type" value="new_student" checked>
											<span> New Student </span> 
										</label>
										<label class="radio inline"> 
											<input type="radio" name="enrollment_type" value="old_student">
											<span> Old Student </span> 
										</label>
										<label class="radio inline"> 
											<input type="radio" name="enrollment_type" value="transferee">
											<span> Transferee </span> 
										</label>
									</div>
								</div> -->

								<div class="form-group">
									<select class="form-control selection" name="enrollment_type">
										<option value="" class="hidden" selected disabled> Please select Enrollment Type</option>
										<option value="new_student" {{ (old('enrollment_type') == 'new_student') ? 'selected' : '' }}>New Student</option>
										<option value="old_students" {{ (old('enrollment_type') == 'old_students') ? 'selected' : '' }}>Old Student</option>
										<option value="transferee" {{ (old('enrollment_type') == 'transferee') ? 'selected' : '' }}>Transferee</option>
									</select>
								</div>
								<div class="form-group">
									{{ Form::text('first_name', '', ['class' => 'form-control', 'placeholder' => 'First Name *']) }}
								</div>
								<div class="form-group">
									{{ Form::text('middle_name', '', ['class' => 'form-control', 'placeholder' => 'Middle Name']) }}
								</div>
								<div class="form-group">
									{{ Form::text('last_name', '', ['class' => 'form-control', 'placeholder' => 'Last Name *']) }}
								</div>
								<div class="form-group">
									{{ Form::text('birth_date', '', ['class' => 'form-control', 'placeholder' => 'Date of Birth * (ex. 05/06/2020 - mm/dd/yyyy)', 'id' => 'birthday', 'autocomplete' => 'off', 'readonly' => 'readonly']) }}
								</div>
								<div class="form-group">
									{{ Form::number('age', '', ['class' => 'form-control', 'placeholder' => 'Your Age *']) }}
								</div>
								<div class="form-group">
									{{ Form::text('religion', '', ['class' => 'form-control', 'placeholder' => 'Religion']) }}
								</div>
								<div class="form-group">
									{{ Form::text('name_of_parent', '', ['class' => 'form-control', 'placeholder' => 'Name of Parent/Guardian *']) }}
								</div>

								<br>

								<!-- School data -->
								<div class="form-group">
									{{ Form::text('previous_school', '', ['class' => 'form-control', 'placeholder' => 'Previous School *']) }}
								</div>

								<div class="form-group">
									<select class="form-control selection" name="year_level" id="year_level">
										<option value="" class="hidden" selected disabled> Please select Year Level</option>
										<option value="year_level_11" {{ (old('year_level') == 'year_level_11') ? 'selected' : '' }} >Year Level 11</option>
										<option value="year_level_12" {{ (old('year_level') == 'year_level_12') ? 'selected' : '' }} >Year Level 12</option>
									</select>
								</div>

								<div class="form-group">
									<select class="form-control selection" name="semester" id="semester">
										<option value="" class="hidden" selected disabled> Please select Semester</option>
										<option value="1st_semester" {{ (old('semester') == '1st_semester') ? 'selected' : '' }} >1st Semester</option>
										<option value="2nd_semester" {{ (old('semester') == '2nd_semester') ? 'selected' : '' }} >2nd Semester</option>
									</select>
								</div>

								<div class="form-group">
									<select class="form-control selection" name="campus">
										<option value="" class="hidden" selected disabled> Please select Campus</option>
										<option value="JPI Santa Maria Campus" {{ (old('campus') == 'JPI Santa Maria Campus') ? 'selected' : '' }} >JPI Santa Maria Campus</option>
										<option value="JPI San jose Campus" {{ (old('campus') == 'JPI San jose Campus') ? 'selected' : '' }} >JPI San jose Campus</option>
										<option value="JPI Muzon Campus" {{ (old('campus') == 'JPI Muzon Campus') ? 'selected' : '' }} >JPI Muzon Campus</option>
										<option value="JPI Plaridel Campus" {{ (old('campus') == 'JPI Plaridel Campus') ? 'selected' : '' }} >JPI Plaridel Campus</option>
									</select>
								</div>

								<br>

								<div class="form-group">
									<div class="upload-file">
										<input id="fileupload_image" type="file" name="image[]">

										<div class="list-image"></div>
									</div>
									
									<small class="text-muted">Please attach your 2x2 picture</small>
									<small class="text-muted">(5mb maximum size per image)</small>
								</div>

								<br>

								<div class="form-group">
									<div class="upload-file">
										<input id="fileupload_grades" type="file" name="grades[]" multiple>

										<div class="list-grades"></div>
									</div>
									
									<small class="text-muted">Please attach your Form 138</small>
									<small class="text-muted">(5mb maximum size per image)</small>
								</div>


							</div>

							<!-- Column Two -->
							<div class="col-md-6">
								<div class="form-group">
									<select class="form-control selection" name="gender">
										<option value="" class="hidden" selected disabled> Please select your Gender</option>
										<option value="male" {{ (old('gender') == 'male') ? 'selected' : '' }} >Male</option>
										<option value="female" {{ (old('gender') == 'female') ? 'selected' : '' }} >Female</option>
									</select>
								</div>
								<div class="form-group">
									{{ Form::email('email', '', ['class' => 'form-control', 'placeholder' => 'Email Address *']) }}
								</div>
								<div class="form-group">
									{{ Form::number('phone_number', '', ['class' => 'form-control', 'placeholder' => 'Phone Number * (ex. 09099876543)']) }}
								</div>
								<div class="form-group">
									{{ Form::text('address', '', ['class' => 'form-control', 'placeholder' => 'Complete Address *']) }}
								</div>
								<div class="form-group">
									{{ Form::text('birth_place', '', ['class' => 'form-control', 'placeholder' => 'Place of Birth (City/Town or Province)']) }}
								</div>
								<div class="form-group">
									<select class="form-control selection" name="civil_status">
										<option value="" class="hidden" selected disabled>Please select your Civil Status</option>
										<option value="Single" {{ (old('civil_status') == 'Single') ? 'selected' : '' }} >Single</option>
										<option value="Married" {{ (old('civil_status') == 'Married') ? 'selected' : '' }} >Married</option>
										<option value="Widowed" {{ (old('civil_status') == 'Widowed') ? 'selected' : '' }} >Widowed</option>
									</select>
								</div>
								<div class="form-group">
									{{ Form::text('nationality', '', ['class' => 'form-control', 'placeholder' => 'Nationality *']) }}
								</div>
								<div class="form-group">
									{{ Form::number('parent_number', '', ['class' => 'form-control', 'placeholder' => 'Parent/Guardian Phone Number * (ex. 09099876543)']) }}
								</div>

								<br>

								<!-- School data -->
								<div class="form-group">
									{{ Form::number('lrn_number', '', ['class' => 'form-control', 'placeholder' => 'LRN Number']) }}
								</div>

								<div class="form-group">
									{{ Form::text('question', '', ['class' => 'form-control', 'placeholder' => 'Comment if you have question']) }}
								</div>

								<div class="form-group">
									<select class="form-control selection inline-select" name="strand" id="strand">
										<option value="" class="hidden" selected disabled> Please select Strand</option>
										<option value="GAS" {{ (old('strand') == 'GAS') ? 'selected' : '' }} >GAS</option>
										<option value="ABM" {{ (old('strand') == 'ABM') ? 'selected' : '' }} >ABM</option>
										<option value="TVL" {{ (old('strand') == 'TVL') ? 'selected' : '' }} >TVL (ICT)</option>
										<option value="HUMSS" {{ (old('strand') == 'HUMSS') ? 'selected' : '' }} >HUMSS</option>
										<option value="STEM" {{ (old('strand') == 'STEM') ? 'selected' : '' }} >STEM</option>
									</select>

									<div class="right-button">
										<button class="btn btn-outline-secondary btn-sm" type="button" data-toggle="modal" data-target="#subjects">Subjects</button>
									</div>
								</div>

								<br>
								<br>
								<br>	

								<div class="form-group">
									<div class="upload-file">
										<input id="fileupload_documents" type="file" name="documents[]" multiple>
										<div class="list-documents"></div>
									</div>

									<small class="text-muted">Please attach the requirement files. <a href="/instructions">Instructions</a></small>
									<small class="text-muted">(5mb maximum size per image)</small>
								</div>

								<input type="submit" class="btnRegister"  value="Submit"/>
							</div>
						</div>
					</div>	
				</div>
			</form>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="subjects" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h6 class="modal-title" id="exampleModalLabel">Subjects</h6>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">

      		<!-- Grade 11 -->
      		<!-- FIRST SEMESTER -->

	      	<div id="year_level_11" class="hidden">

		      	<div id="1st_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

						  	<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Business Mathematics</li>
							<li class="list-group-item">Organization and Management</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

						  	<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>

						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Pre-Calculus</li>
							<li class="list-group-item">General Biology 1</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>


						  	<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

						  	<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disciplines and Ideas in the Social Sciences</li>
						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Organization and Management</li>
							<li class="list-group-item">Elective 1</li>
						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Oral Communication in Context</li>
							<li class="list-group-item">Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino</li>
							<li class="list-group-item">General Mathematics</li>
							<li class="list-group-item">Earth and Life Science</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>

	      		<!-- Grade 11 -->
	      		<!-- Second SEMESTER -->

		      	<div id="2nd_semester" class="semester-target"> 

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Fundamentals of Accountancy, Business and Management 1</li>
							<li class="list-group-item">Principles of Marketing</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Basic Calculus</li>
							<li class="list-group-item">General Biology 2</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disciplines and Ideas in the Applied Social Sciences</li>
							<li class="list-group-item">Creative Writing/Malikhaing Pagsulat</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Disaster Readiness and Risk Reduction</li>
							<li class="list-group-item">HUMSS 1:Creative Writing/Malikhaing Pagsulat</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Reading and Writing Skills</li>
							<li class="list-group-item">Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik</li>
							<li class="list-group-item">Statistics and Probability</li>
							<li class="list-group-item">Physical Science</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Filipino sa Piling Larangan (Akademik)</li>
							<li class="list-group-item">Practical Research 1</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>
							<!-- <li class="list-group-item">Computer System Servicing (NC II)</li> -->

						</ul>

			      	</div>
		      	</div>

	      	</div>

      		<!-- Grade 12 -->
      		<!-- First SEMESTER -->

	      	<div id="year_level_12" class="hidden">

		      	<div id="1st_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Fundamentals of Accountancy, Business and Management 2</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">General Physics 1</li>
							<li class="list-group-item">General Chemistry 1</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<!-- <li class="list-group-item">21st Century Literature from the Philippines and the World</li> -->
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Introduction to World Religions and Belief Systems</li>
							<li class="list-group-item">Philippine Politics and Governance</li>
							<li class="list-group-item">Creative Nonfiction</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<!-- <li class="list-group-item">21st Century Literature from the Philippines and the World</li> -->
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Entrepreneurship</li>
							<li class="list-group-item">Practical Research 2</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">HUMSS 2: Introduction to World Religions and Belief Systems</li>
							<li class="list-group-item">Philippine Politics and Governance</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) 1st Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">21st Century Literature from the Philippines and the World</li>
							<li class="list-group-item">Introduction to the Philosophy of the Human Person</li>
							<li class="list-group-item">Understanding Culture, Society and Politics</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>
							<li class="list-group-item">Physical Education and Health</li>
							<li class="list-group-item">Japanese language</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">English for Academic and Professional Purposes</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>

	      		<!-- Grade 12 -->
	      		<!-- Second SEMESTER -->

		      	<div id="2nd_semester" class="semester-target">

			      	<div class="ABM subject-target">

						<h3  class="header-subject">ABM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Applied Economics</li>
							<li class="list-group-item">Business Ethics and Social Responsibility</li>
							<li class="list-group-item">Business Finance</li>
							<li class="list-group-item">Business Enterprise Simulation</li>

						</ul>
						
			      	</div>

			      	<div class="stem subject-target">

						<h3  class="header-subject">STEM STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>
							<li class="list-group-item">Contemporary Philippine Arts from the Regions</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">General Physics 2</li>
							<li class="list-group-item">General Chemistry 2</li>
							<li class="list-group-item">Research/Capstone Project</li>

						</ul>
			      		
			      	</div>

			      	<div class="HUMSS subject-target">
			      		
			      		<h3  class="header-subject">HUMSS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Trends, Networks, and Critical Thinking in the 21st Century</li>
							<li class="list-group-item">Community Engagement, Solidarity, and Citizenship (CSC)</li>
							<li class="list-group-item">Culminating Activity</li>

						</ul>

			      	</div>

			      	<div class="GAS subject-target">
			      		
			      		<h3  class="header-subject">GAS STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Applied Economics</li>
							<li class="list-group-item">Elective 2 (from any Track/Strand)</li>
							<li class="list-group-item">Work Immersion/Culminating Activity</li>

						</ul>

			      	</div>

			      	<div class="TVL subject-target">
			      		
			      		<h3  class="header-subject">TVL (ICT) STRAND 2nd Semester</h3>

						<ul class="list-group list-group-flush edit-style">

							<div class="sub-title">CORE SUBJECTS</div>
							<li class="list-group-item">Media and Information Literacy</li>
							<li class="list-group-item">Personal Development</li>
							<li class="list-group-item">PHYSICAL EDUCATION AND HEALTH</li>
							<li class="list-group-item">Work Immersion</li>

							<div class="sub-title">APPLIED SUBJECTS</div>
							<li class="list-group-item">Empowerment Technologies (for the Strand)</li>
							<li class="list-group-item">Inquiries, Investigations and Immersion</li>

							<div class="sub-title">SPECIALIZED SUBJECTS</div>
							<li class="list-group-item">Computer System Servicing (NC II)</li>

						</ul>

			      	</div>
		      	</div>
		      	
	      	</div>


	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-outline-secondary btn-sm" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

</div>

@endsection


@section('script')

@endsection