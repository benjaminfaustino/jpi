@extends('layouts.vendor')

@section('title', 'Instruction')

@section('style')

@endsection

@section('content')

<div class="header-hero"></div>

<div class="container register">
	
	<div class="row">
		<div class="col-md-3 register-left">
			<!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/> -->
			<h3>Welcome</h3>
			<p>This is JPI Technologies Enrollment Form. Please see the instruction</p>

<!-- 			<ul class="list-group list-group-flush edit-style">
			  <li class="list-group-item contact-details">
			  	<p class="network">Globe</p> <p class="net-number"> - 09171388348</p> <br>
			  	<p class="network">TM</p> <p class="net-number"> - 09351734864 </p>
			  </li>
			  <li class="list-group-item contact-details">
				<p class="network">TNT</p> <p class="net-number"> - 09502486125 </p> <br>
				<p class="network">Sun</p> <p class="net-number"> - 09335299474 </p>
			  </li>
			  <li class="list-group-item">
			  	<p class="network">Facebook Page</p> - <a href="https://www.facebook.com/JpiTechnologies/">https://www.facebook.com/JpiTechnologies/</a>
			  </li>
			  <li class="list-group-item"></li>
			</ul> -->

			<a href="/enrollment-form">
				<input type="submit" name="" value="Enroll Now"/><br/>
			</a>

			<br><br><br><br>

			<h4>Contact Details</h4>

			<br>

  			<p class="network">Globe</p>  
  			<br>
  			(+63) 0917-138-8348 
  			<br>
			<p class="network">TM</p>
			<br>  
			(+63) 0935-173-4864 

			<br><br>

			<p class="network">TNT</p>  
			<br>
			(+63) 0950-248-6125
			<br>

			<p class="network">Sun</p>
			<br>
			(+63) 0933-529-9474 

			<br><br>

		  	<p class="network">Facebook Page</p>
		  	<a href="https://www.facebook.com/JpiTechnologies/" class="school-fb-size">https://www.facebook.com/JpiTechnologies/</a>


		</div>
		<div class="col-md-9 register-right">
			<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
<!-- 				<li class="nav-item">
					<a class="nav-link active" id="instruction-tab" data-toggle="tab" href="#instruction" role="tab" aria-controls="instruction" aria-selected="true">Instruction</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="enrollment-form-tab" data-toggle="tab" href="#enrollment-form" role="tab" aria-controls="enrollment-form" aria-selected="false">Form</a>
				</li> -->
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="instruction" role="tabpanel" aria-labelledby="instruction-tab">
					<h3  class="register-heading">Instructions</h3>
					<div class="row register-form">

						<p class="reminder">
							For the purposes of enrollment and overall student management, we obtain information from you as an enrollee. 
							By submitting your information, you give us your permission to use your information for the purposes set out above 
							at the same time giving your full consent (student and guardian) in joining our school. After completion of the enrollment 
							and of all necessary documentation, your personal data will be stored for future reference and marketing activities in secure files. 
							If you do not wish to be notified through cancelation or after this case, please notify us through <a href="Jpitechnologies2016@gmail.com">Jpitechnologies2016@gmail.com</a>.
						</p>

					</div>

					<h3  class="register-heading">Requirements</h3>
					<div class="row register-form">

						<ul class="list-group list-group-flush edit-style">
						  <li class="list-group-item">Original Copy of Birth Certificate (PSA)</li>
						  <li class="list-group-item">Original Copy of Form 137 (Permanent Record)</li>
						  <li class="list-group-item">Original Copy of Form 138 (Grade 10 Report Card)</li>
						  <li class="list-group-item">Original Copy of Good Moral</li>
						  <li class="list-group-item">Photocopy of National Career Assessment Examination (NCAE)</li>
						  <li class="list-group-item">2 pcs 2 by 2 picture</li>
						  <li class="list-group-item"></li>
						</ul>
						
					</div>

					<h3  class="register-heading">Online Enrollment Procedure</h3>
					<div class="row register-form">

						<!-- <p class="reminder">Kindly follow the procedure respectively:</p> -->
						<br>

						<ul class="list-group list-group-flush edit-style">
						  <li class="list-group-item">Visit our Online Enrollment Portal <a href="/enrollment-form">jpi-tech.com/enrollment-form</a></li>
						  <li class="list-group-item">Fill-out the Pre-enrollment Form. Be sure that all pertinent information is true and correct.</li>
						  <li class="list-group-item">Upload the available documents you have like Birth Certificate (if available) and report card (if available).</li>
						  <li class="list-group-item">Click the submit button.</li>
						  <li class="list-group-item">Once you submit, you will be receiving a notification of your enrollment form through your email or messenger within five to seven working days.</li>
						  <li class="list-group-item">No payment involved.</li>
						  <li class="list-group-item"></li>
						</ul>
						
					</div>

				</div>
		</div>
	</div>

</div>

@endsection


@section('script')

@endsection