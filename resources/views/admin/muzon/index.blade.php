@extends('layouts.admin')

@section('title', 'Muzon Branch')

@section('stylesheets')

@section('content')

  <div class="content-wrapper">

  <!-- Get data from this route value /GetEnrollmentList-->
  <input id="get-value" type="hidden" value="MuzonList">
  <input type="hidden" :value="collections">

	<!-- Main content -->
    <section class="content hidden">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="box-title col-md-12">
                  <select2 class="search inline-list form-control" :key="reference">
                      <option></option>
                      <option v-for="student in students" :value="student.id">@{{ student.last_name }} @{{ student.first_name }}</option>
                  </select2>

                  <select class="search inline-list form-control" v-model="strandDefault">
                      <option v-for="strand in strandList" :value="strand.data">@{{ strand.display }}</option>
                  </select>

                  <select class="search inline-list form-control" v-model="levelDefault">
                      <option v-for="level in grade_level" :value="level.data">@{{ level.display }}</option>
                  </select>

                    <select class="search inline-list form-control" v-model="semesterDefault">
                      <option v-for="semester in grade_semester" :value="semester.data">@{{ semester.display }}</option>
                  </select>

                  <div class="create-button inline-list">
                      <button 
                        type="button" 
                        class="btn btn-info btn-flat"  
                        data-toggle="modal" 
                        data-target="#modal-app"
                        @click="add()">
                          <i class="fas fa-plus"></i>
                      </button>
                  </div>
              </div>

              <div class="count-div">
                
                <span class="total-count">Total Students: @{{ dataList.length }}</span>
                <span class="total-count"># Enrolled on this Strand/Year/Semester: @{{ students.length }}</span>

              </div>

            </div>

            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <!-- <th>Address</th> -->
                  <th>CP #</th>
                  <!-- <th>Email</th> -->
                  <th>Birthday</th>
                  <th>Gender</th>
                  <th class="center">Print Status</th>
                  <th>Menu</th>
                </tr>
                </thead>
                <!-- <tbody id="sortable"> -->
                  <tbody>

                  <!-- (item, index) in orderBy(collections) -->

                	<tr v-for="(item, student_index) in students" class="collection-listing  ui-state-default" :id="item.id">
                    <td class="td-width image"> 

                      <img v-if="(item.gender == 'male') " src="/assets/images/male-placeholder.jpg">
                      <img v-if="(item.gender == 'female') " src="/assets/images/female-placeholder.jpg">
                      
                      <!-- <img v-if="item.image != 'placeholder.png' " :src="item.image">                       -->

                    </td> 
                    <td class="td-width"> @{{ item.last_name }}, @{{ item.first_name }} @{{ item.middle_name }}
                    </td>
      <!--               <td class="td-width">
                      @{{ item.address }}
                    </td> -->
                    <td class="td-width">
                      @{{ item.phone_number }}
                    </td>
                    <td class="td-width">
                      @{{ item.birth_date }}
                    </td>
                    <td class="td-width capitalize">
                      @{{ item.gender }}
                    </td>

                    <td class="td-width center" :class="{ completed : item.print_status, 'in-complete': !item.print_status }">
                      <div class="btn-group">

                        <button 
                          type="button" 
                          class="btn btn-success btn-flat"
                          @click="updatePrintStatus(item, student_index)">
                            <i class="fas fa-print"></i>
                        </button>

                      </div>
                    </td>

                    <td class="td-width center">
                      <div class="btn-group">

                        <button 
                          type="button" 
                          class="btn btn-info btn-flat"  
                          data-toggle="modal" 
                          data-target="#modal-app"
                          @click="edit(item, student_index)">
                            <i class="fas fa-edit"></i>
                        </button>

                        @if(Auth::user()->email == "jpi@admin.com")

                          <button 
                            type="button" 
                            class="btn btn-danger btn-flat"
                            @click="remove(item, student_index)">
                              <i class="fas fa-trash-alt"></i>
                          </button>

                        @endif

                        <a :href=" '/admin/print/' + item.id" target="_blank">
                          <button 
                            type="button" 
                            class="btn btn-success btn-flat">
                              <i class="fas fa-print"></i>
                          </button>
                        </a>

                      </div>
                    </td>
                  </tr>

            	   </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
  		  </div>
  	  </div>
  	</section>

    <div id="modal-app" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Member Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="">
              <div class="box-body" :key="reference">

                <div class="col-md-4">

                  <div class="form-group">
                    <label>Ernollment type</label>
                    <select class="form-control selection" name="enrollment_type" v-model="collect.enrollment_type">
                      <option value="" class="hidden" selected disabled> Please select Enrollment Type</option>
                      <option value="new_student" {{ (old('enrollment_type') == 'new_student') ? 'selected' : '' }}>New Student</option>
                      <option value="old_students" {{ (old('enrollment_type') == 'old_students') ? 'selected' : '' }}>Old Student</option>
                      <option value="transferee" {{ (old('enrollment_type') == 'transferee') ? 'selected' : '' }}>Transferee</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>First name</label>
                    <input type="text" class="form-control" v-model="collect.first_name">
                  </div>

                  <div class="form-group">
                    <label>Middle name</label>
                    <input type="text" class="form-control" v-model="collect.middle_name">
                  </div>

                  <div class="form-group">
                    <label>Last name</label>
                    <input type="text" class="form-control" v-model="collect.last_name">
                  </div>

                  <div class="form-group">
                    <label>Birth Date</label>
                    <input type="text" class="form-control" v-model="collect.birth_date" id="birthday">
                  </div>

                  <div class="form-group">
                    <label>Age</label>
                    <input type="number" class="form-control" v-model="collect.age">
                  </div>

                  <div class="form-group">
                    <label>Nationality</label>
                    <input type="text" class="form-control" v-model="collect.nationality">
                  </div>

                  <div class="form-group">
                    <label>Strand</label>
                    <select class="form-control selection" name="strand" id="strand" v-model="collect.strand">
                      <option value="" class="hidden" selected disabled> Please select Strand</option>
                      <option value="GAS" {{ (old('strand') == 'GAS') ? 'selected' : '' }} >GAS</option>
                      <option value="ABM" {{ (old('strand') == 'ABM') ? 'selected' : '' }} >ABM</option>
                      <option value="TVL" {{ (old('strand') == 'TVL') ? 'selected' : '' }} >TVL (ICT)</option>
                      <option value="HUMSS" {{ (old('strand') == 'HUMSS') ? 'selected' : '' }} >HUMSS</option>
                      <option value="STEM" {{ (old('strand') == 'STEM') ? 'selected' : '' }} >STEM</option>
                    </select>
                  </div>

                  <br>

<!--                   <div class="form-group">
                    <div class="upload-file">
                      <input id="fileupload_image" type="file" name="image[]">

                      <div class="list-image"></div>
                    </div>
                    
                    <small class="text-muted">Please attach your 2x2 picture</small>
                  </div>

                  <div class="form-control">
                    
                    <template v-for="document in collect.documents">
                      
                      <template v-if="document.type == 'grades'">
                        <img :src=" '/uploads/grades/' + document.uuid + '.png' ">
                      </template>
                      <template v-if="document.type == 'documents'">
                        <img :src=" '/uploads/documents/' + document.uuid + '.png' ">
                      </template>

                    </template>

                  </div> -->

                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4">
                  
                  <div class="form-group">
                    <label>Religion</label>
                    <input type="text" class="form-control" v-model="collect.religion">
                  </div>

                  <div class="form-group">
                    <label>Name of Parent/Guardian</label>
                    <input type="text" class="form-control" v-model="collect.name_of_parent">
                  </div>

                  <div class="form-group">
                    <label>Year Level</label>
                    <select class="form-control selection" name="year_level" id="year_level"  v-model="collect.year_level">
                      <option value="" class="hidden" selected disabled> Please select Year Level</option>
                      <option value="year_level_11" {{ (old('year_level') == 'year_level_11') ? 'selected' : '' }} >Year Level 11</option>
                      <option value="year_level_12" {{ (old('year_level') == 'year_level_12') ? 'selected' : '' }} >Year Level 12</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Previous School</label>
                    <input type="text" class="form-control" v-model="collect.previous_school">
                  </div>

                  <div class="form-group">
                    <label>Semester</label>
                    <select class="form-control selection" name="semester" id="semester" v-model="collect.semester">
                      <option value="" class="hidden" selected disabled> Please select Semester</option>
                      <option value="1st_semester" {{ (old('semester') == '1st_semester') ? 'selected' : '' }} >1st Semester</option>
                      <option value="2nd_semester" {{ (old('semester') == '2nd_semester') ? 'selected' : '' }} >2nd Semester</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Campus</label>
                    <select class="form-control selection" name="campus" v-model="collect.campus">
                      <option value="" class="hidden" selected disabled> Please select Campus</option>
                      <option value="JPI Santa Maria Campus" {{ (old('campus') == 'JPI Santa Maria Campus') ? 'selected' : '' }} >JPI Santa Maria Campus</option>
                      <option value="JPI San jose Campus" {{ (old('campus') == 'JPI San jose Campus') ? 'selected' : '' }} >JPI San jose Campus</option>
                      <option value="JPI Muzon Campus" {{ (old('campus') == 'JPI Muzon Campus') ? 'selected' : '' }} >JPI Muzon Campus</option>
                      <option value="JPI Plaridel Campus" {{ (old('campus') == 'JPI Plaridel Campus') ? 'selected' : '' }} >JPI Plaridel Campus</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Parent/Guardian Phone Number</label>
                    <input type="text" class="form-control" v-model="collect.parent_number">
                  </div>

                  <br>

<!--                   <div class="form-group">
                    <div class="upload-file">
                      <input id="fileupload_grades" type="file" name="grades[]" multiple>

                      <div class="list-grades"></div>
                    </div>
                    
                    <small class="text-muted">Please attach your Form 138</small>
                  </div> -->

                </div>
                <!-- End col-md-4 -->

                <div class="col-md-4">

                  <div class="form-group">
                    <label>Gender</label>
                    <select class="form-control selection" name="gender" v-model="collect.gender">
                      <option value="" class="hidden" selected disabled> Please select your Gender</option>
                      <option value="male" {{ (old('gender') == 'male') ? 'selected' : '' }} >Male</option>
                      <option value="female" {{ (old('gender') == 'female') ? 'selected' : '' }} >Female</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>Email</label>
                    <input type="email" class="form-control" v-model="collect.email">
                  </div>

                  <div class="form-group">
                    <label>Phone Number</label>
                    <input type="text" class="form-control" v-model="collect.phone_number">
                  </div>

                  <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control" v-model="collect.address">
                  </div>

                  <div class="form-group">
                    <label>Birth Place</label>
                    <input type="text" class="form-control" v-model="collect.birth_place">
                  </div>

                  <div class="form-group">
                    <label>Civil Status</label>
                    <select class="form-control selection" name="civil_status" v-model="collect.civil_status">
                      <option value="" class="hidden" selected disabled>Please select your Civil Status</option>
                      <option value="Single" {{ (old('civil_status') == 'Single') ? 'selected' : '' }} >Single</option>
                      <option value="Married" {{ (old('civil_status') == 'Married') ? 'selected' : '' }} >Married</option>
                      <option value="Widowed" {{ (old('civil_status') == 'Widowed') ? 'selected' : '' }} >Widowed</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label>LRN Number</label>
                    <input type="text" class="form-control" v-model="collect.lrn_number">
                  </div>

                  <br>

                  <div class="form-group date_enrolled">
                    <div :id="date.id" class="date_enrolled_li" v-for="date in collect.dates">
                      @{{ date.school_year }} - @{{ date.created_at }}

                      <a class="pull-right clickable" @click="dateEnrolled(date)">
                        <i class="fas fa-minus-square red"></i>
                      </a>
                        
                      </i>
                    </div>
                  </div>

<!--                   <div class="form-group">
                    <div class="upload-file">
                      <input id="fileupload_documents" type="file" name="documents[]" multiple>
                      <div class="list-documents"></div>
                    </div>

                    <small class="text-muted">Please attach the requirement files. <a href="/instructions">Instructions</a></small>
                  </div> -->

                </div>
                <!-- End of col-md-4 -->

              </div>
              <!-- /.box-body -->
            </div>

          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-flat" data-dismiss="modal">Cancel</button>

            <template v-if="buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="save()">Save</button>
            </template>
            <template v-if="!buttonActive">
              <button type="button" class="btn btn-primary btn-flat" @click="update()">Update</button>
            </template>

          </div>
        </div>
      </div>
    </div>

  </div>

@endsection

@section('scripts')
	
	<script type="text/javascript" src="{{ asset('/assets/js/enrollment/function.js') }}"></script>

@stop