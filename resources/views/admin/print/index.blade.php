@extends('layouts.print')

@section('title', 'Print Data')

@section('stylesheets')

<style type="text/css">

  .wrapper {
    position: relative;
    overflow-x: unset;
    overflow-y: unset;
  }

  .school-brand {
    display: block;
    height: 440px;
  }

  .school-name {
    writing-mode: vertical-rl;
    text-orientation: use-glyph-orientation;
    transform: rotate(-180deg);
    position: relative;
    left: 20px;
    top: 30px;
    text-align: center;
    width: 100px;
  }

  .logo {
    transform: rotate(-90deg);
    width: 80px;
    height: 80px;
    position: relative;
    bottom: -48px;
    left: 23px;
  }

  .parent-content {
    border: 1px solid;
  }

  .name {
    color: #144d35;
    font-weight: 600;
  }

  .text-muted {
    color: #62736b;
    font-size: 10px;
    font-weight: 600;
  }

  .copy-text {
    font-weight: 700;
    font-size: 14px;
    left: -33px;
    position: relative;
  }

  .school-name, .form-content {
    display: inline-block;
  }

  .form-content {
    position: absolute;
    width: 750px;
    height: calc(100% - 850px);
    margin: 0 0 0 20px;
  }

  .form-title {
    text-align: center;
    font-weight: 500;
    color: #ffffff;
    background: #000000;
    padding: 5px;
    border-bottom: 1px solid;
    font-size: 13px;
  }

  .border {
    display: inline-block;
    height: 30px;
    border-right: 1px solid;
    border-bottom: 1px solid;
    margin-right: -4px;
    margin-bottom: -6px;
    font-size: 10px;
    line-height: 42px;
    padding: 0 5px;
    font-weight: 700;
  }

  .student-number,
  .student-lrn {
    width: 19%;
  }

  .student-name {
    width: 40%;
  }

  .student-level {
    width: 12%;
  }

  .student-status-type {
    width: 15%;
  }

  .student-pic {
    border-bottom: unset;
    width: 14% !important;
  }

  .less-bottom {
    border-bottom: unset !important;
  }

  .student-track {
    width: 10.2%;
  }

  .student-type,
  .student-gender
   {
    width: 19%;
  }

  .student-birth {
    width: 18.8%;
  }

  .student-age {
    width: 5%;
  }

  .student-sem {
    width: 20%;
  }

  .student-home-address,
  .student-place-birth
   {
    white-space: nowrap;
    width: 281.2px;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .student-pic-border {
    border-bottom: 1px solid;
    width: 14%;
  }

  .student-status {
    width: 12%;
  }

  .student-nationality {
    width: 13%;
  }

  .student-religion,
  .student-email {
    width: 37.5%
  }

  .less-right {
    border-right: unset;
  }

  .student-school
   {
    white-space: nowrap;
    width: 281.2px;
    overflow: hidden;
    text-overflow: ellipsis;
  }


  .student-school,
  .student-address,
  .student-contact {
    width: 33.4%;
  }

  .student-address
   {
    white-space: nowrap;
    width: 250px;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .student-subjects, 
  .student-days,
  .student-time,
  .student-room, 
  .student-tuition, 
  .student-date-enrolled,
  .student-payment-details,
  .student-tuition {
    text-align: center;
    line-height: 30px;
    background: #000000;
    color: #ffffff;
    font-size: 9px;
    height: 26px;
  }

  .student-payment-details {
    width: 14.6%;
  }

  .student-subjects,
  .student-subjects-sub {
    width: 40%;
  }

  .student-subjects-sub {
    font-size: 10px;
    font-style: bold;
  }

  .student-days,
  .student-time,
  .student-room,
  .student-days-sub,
  .student-time-sub,
  .student-room-sub {
    width: 6%;
  }

  .student-tuition,
  .student-tuition-sub {
    width: 16% !important;
  }

  .student-payment-details-sub {
    width: 14.7%;
  }

  .student-date-enrolled-sub {
    width: 11.6%;
  }

  .student-date-enrolled {
    width: 12%;
  }

  .student-subjects-sub,
  .student-days-sub,
  .student-time-sub,
  .student-room-sub,
  .student-tuition-sub,
  .student-date-enrolled-sub,
  .student-payment-details-sub {
    height: 176px;
  }

  .student-signature,
  .student-parent,
  .student-contact,
  .student-registrar,
  .student-admin,
  .student-staff {
    width: 33.4%;
  }

  .custom-height {
    height: 30px;
    line-height: 43px;
  }

  .line {
    margin: -1.8px;
    padding: 0;
    line-height: 26px;
    border-bottom: 1px solid;
    height: 19px;
  }

  .student-subjects-sub .line {
    white-space: nowrap;
    width: 292px;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  label {
    font-size: 10px;
    position: absolute;
    display: block;
    margin: 0;
    padding: 0;
    line-height: 18px;
  }

  .label-fee,
  .label-misce,
  .label-others,
  .label-line
  {
    position: absolute;
  }

  .fee,
  .fee-misce,
  .fee-others,
  .fee-total,
  .fee-line {
    position: absolute;
    right: 26.5%;
  }

  .label-fee {
    line-height: 100px;
  }

  .label-misce {
    line-height: 130px;
  }

  .label-others {
    line-height: 160px;
  }

  .fee {
    line-height: 100px;
  }

  .fee-misce {
    line-height: 130px;
  }

  .fee-others {
    line-height: 160px;
  }

  .fee-line {
    line-height: 170px;
  }

  .fee-total {
    line-height: 200px;
  }

  .date-enrolled {
    position: absolute;
    right: 16.5%;
  }

  .border-line {
    position: absolute;
    line-height: 110px;
  }

  .payment-qualified,
  .payment-recipient {
    position: absolute;
    font-size: 10px;
  }

  .payment-or, 
  .payment-date,
  .payment-amount {
    position: absolute;
  }

  .payment-recipient {
    line-height: 72px;
  }

  .payment-or {
    line-height: 140px;
  } 

  .payment-date {
    line-height: 210px;
  }

  .payment-amount {
    top: 320px;
  }

  .custom-place {
    position: relative;
    left: 34%;
    transform: rotate(-270deg);
    bottom: 421px;
  }

  .adjust-top {
    top: 55px;
    position: relative;
  }

  .image-pic {
    position: absolute;
    top: 40px;
    font-size: 55px;
    right: 7px;
    color: grey;
  }

  .student-tuition-sub {
    font-size: 10px;
  }

  input {
    width: 8px;
    height: 8px;
  }

  .padding-top {
    padding-bottom: 5px;
  }

</style>

@endsection

@section('content')

  <!-- Registrar's copy -->
  <div class="school-brand">
    
    <div class="school-name">
      
      <p class="name">Japan-Philippines Institute of Technologies</p>

      @if(strtolower($student->campus) == "jpi san jose campus")

      <p class="text-muted">JAG bldg EVR Road purok 7 , Brgy San pedro, </p>
      <p class="text-muted">Area E. Sapang Palay  City of San Jose Del Monte</p>

      @endif

      @if(strtolower($student->campus) == "jpi plaridel campus")

      <p class="text-muted">Rocka commercial complex, Cagayan valley National Road,</p>
      <p class="text-muted">Tabang, Plaridel, Bulacan.</p>

      @endif

      @if(strtolower($student->campus) == "jpi santa maria campus")

      <p class="text-muted">3/F JM Squre Bldg. Poblacion, Santa Maria, Bulacan</p>

      @endif

      @if(strtolower($student->campus) == "jpi muzon campus")

      <p class="text-muted">M. Villarica Provincial road, zone 1, Muzon,</p>
      <p class="text-muted">city of san jose del monte, Bulacan</p>
      
      @endif

      <p class="copy-text">Registrar's Copy</p>

    </div>

    <div class="form-content">
      
      <div class="parent-content">

        <div class="form-title">
          SENIOR HIGH SCHOOL ENROLLMENT FORM S.Y __________
        </div>

        <div class="student-number border">

          <label>STUDENT NUMBER:</label> 
          
        </div>

        <div class="student-name border">
          
          <label>NAME (LAST, GIVEN, MIDDLE):</label> {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

        </div>

        <div class="student-level border">

          <label>YEAR LEVEL: </label>

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_11') ? 'checked' : '' }}> 11

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_12') ? 'checked' : '' }}> 12
          
        </div>

        <div class="student-status-type border">
            
            <label>STATUS:</label>

            @if($student->enrollment_type == 'new_student')
              New Student
            @endif

            @if($student->enrollment_type == 'old_student')
              Old Student
            @endif

            @if($student->enrollment_type == 'transferee')
              Transferee
            @endif

        </div>

        <div class="student-pic border less-bottom less-right">
          
        </div>


        <!-- Next Line -->

        <div class="student-lrn border">

          <label>LRN:</label> {{ $student->lrn_number }}
          
        </div>

        <div class="student-track border">
          
          <label>TRACK:</label>

          {{ $student->strand }}

        </div>

        <div class="student-type border">

          <label>STUDENT TYPE:</label>

          <input type="checkbox" name="year"> Public

          <input type="checkbox" name="year"> Private
          
        </div>

        <div class="student-gender border">

          <label>GENDER: </label>
          
          <input type="checkbox" name="year" {{ ($student->gender == 'male') ? 'checked' : '' }}> Male

          <input type="checkbox" name="year" {{ ($student->gender == 'female') ? 'checked' : '' }}> Female

        </div>

        <div class="student-birth border">

          <label>BIRTHDAY:</label>

          {{ $student->birth_date }}
          
        </div>

        <div class="student-pic-border border less-right">
          <div class="image-pic">1x1</div>          
        </div>

        <!-- Next line -->

        <div class="student-age border">

          <label>AGE:</label>

          {{ $student->age }}
          
        </div>

        <div class="student-sem border">

          <label>SEMESTER:</label> 
          
          <input type="checkbox" name="year" {{ ($student->semester == '1st_semester') ? 'checked' : '' }}> 1ST SEM

          <input type="checkbox" name="year" {{ ($student->semester == '2nd_semester') ? 'checked' : '' }}> 2ND SEM
          
        </div>

        <div class="student-home-address border">

          <label>COMPLETE ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-place-birth border less-right">

          <label>PLACE OF BIRTH:</label>

          {{ $student->birth_place }}
          
        </div>

        <!-- Next line -->

        <div class="student-status border">

          <label>CIVIL STATUS:</label>

          {{ $student->civil_status }}
          
          
        </div>

        <div class="student-religion border">

          <label>Religion:</label>

          {{ $student->religion }}
          
        </div>

        <div class="student-nationality border">
          
          <label>NATIONALITY:</label>

          {{ $student->nationality }}

        </div>

        <div class="student-email border less-right">

          <label>EMAIL:</label>

          {{ $student->email }}
          
        </div>

        <!-- Next line -->

        <div class="student-school border">

          <label>JUNIOR HIGH SCHOOL:</label>

          {{ $student->previous_school }}
          
        </div>

        <div class="student-address border">

          <label>ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-contact border less-right">
          
          <label>CONTACT NUMBER:</label>

          {{ $student->phone_number }}

        </div>

        <!-- Next line -->

        <div class="student-subjects border">
          SUBJECTS
        </div>

        <div class="student-days border">
          DAYS
        </div>

        <div class="student-time border">
          TIME
        </div>

        <div class="student-room border">
          ROOM
        </div>

        <div class="student-tuition border">
          TUITION FEE
        </div>

        <div class="student-date-enrolled border">
          DATE ENROLLED
        </div>

        <div class="student-payment-details border less-right">
          PAYMENT DETAILS
        </div>

        <!-- Next line -->

        <div class="student-subjects-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-days-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
         
        </div>

        <div class="student-time-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-room-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-tuition-sub border">


          <div class="label-fee">Tuition</div>
          <div class="fee">11,500.00</div> 

          <div class="label-misce">Miscellaneous</div>
          <div class="fee-misce">2,850.00</div> 

          <div class="label-others">Others Fee</div>
          <div class="fee-others">3,150.00</div>

          <div class="fee-line">__________</div>
          <div class="fee-total">17,500.00</div>

        </div>

        <div class="student-date-enrolled-sub border">

          <div class="date-enrolled">
            {{ $date }}
          </div>

          <div class="border-line">Note:</div>

        </div>

        <div class="student-payment-details-sub border less-right">

          <div class="payment-qualified">
            Qualified Voucher
          </div>

          <div class="payment-recipient">Recipient (QVR)</div>

          <div class="payment-or">
            OR NO.
          </div>

          <div class="payment-date">
            Date
          </div>

          <div class="payment-amount">
            Amount
          </div>

        </div>

        <!-- Next line -->

        <div class="student-signature border custom-height custom-height">

          <label>STUDENT'S SIGNATURE:</label>

        </div>

        <div class="student-parent border custom-height custom-height">

          <label>NAME OF PARENT/GUARDIAN:</label>

          {{ $student->name_of_parent }}

        </div>

        <div class="student-contact border custom-height custom-height less-right">

          <label>CONTACT NO. OF PARENT/GUARDIAN:</label>

          {{ $student->parent_number }}

        </div>

        <!-- Next line -->

        <div class="student-registrar border custom-height custom-height less-bottom">

          <label>SCHOOL REGISTRAR:</label>

        </div>

        <div class="student-admin border custom-height custom-height less-bottom">

          <label>SCHOOL ADMINISTRATOR:</label>

        </div>

        <div class="student-staff border custom-height custom-height less-bottom less-right">

          <label>JPI ADMISSION STAFF:</label>

        </div>
        
      </div>

    </div>

    <div class="logo">
      <img src="/assets/images/default.png">
    </div>

  </div>

  <div class="padding-top">
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  </div>


   <!-- Student's copy -->
  <div class="school-brand">
    
    <div class="school-name">
      
      <p class="name">Japan-Philippines Institute of Technologies</p>

      @if(strtolower($student->campus) == "jpi san jose campus")

      <p class="text-muted">JAG bldg EVR Road purok 7 , Brgy San pedro, </p>
      <p class="text-muted">Area E. Sapang Palay  City of San Jose Del Monte</p>

      @endif

      @if(strtolower($student->campus) == "jpi plaridel campus")

      <p class="text-muted">Rocka commercial complex, Cagayan valley National Road,</p>
      <p class="text-muted">Tabang, Plaridel, Bulacan.</p>

      @endif

      @if(strtolower($student->campus) == "jpi santa maria campus")

      <p class="text-muted">3/F JM Squre Bldg. Poblacion, Santa Maria, Bulacan</p>

      @endif

      @if(strtolower($student->campus) == "jpi muzon campus")

      <p class="text-muted">M. Villarica Provincial road, zone 1, Muzon,</p>
      <p class="text-muted">city of san jose del monte, Bulacan</p>
      
      @endif

      <p class="copy-text">Student's Copy</p>

    </div>

    <div class="form-content">
      
      <div class="parent-content">

        <div class="form-title">
          SENIOR HIGH SCHOOL ENROLLMENT FORM S.Y __________
        </div>

        <div class="student-number border">

          <label>STUDENT NUMBER:</label> 
          
        </div>

        <div class="student-name border">
          
          <label>NAME (LAST, GIVEN, MIDDLE):</label> {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

        </div>

        <div class="student-level border">

          <label>YEAR LEVEL: </label>

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_11') ? 'checked' : '' }}> 11

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_12') ? 'checked' : '' }}> 12
          
        </div>

        <div class="student-status-type border">
            
            <label>STATUS:</label>

            @if($student->enrollment_type == 'new_student')
              New Student
            @endif

            @if($student->enrollment_type == 'old_student')
              Old Student
            @endif

            @if($student->enrollment_type == 'transferee')
              Transferee
            @endif

        </div>

        <div class="student-pic border less-bottom less-right">
          
        </div>


        <!-- Next Line -->

        <div class="student-lrn border">

          <label>LRN:</label> {{ $student->lrn_number }}
          
        </div>

        <div class="student-track border">
          
          <label>TRACK:</label>

          {{ $student->strand }}

        </div>

        <div class="student-type border">

          <label>STUDENT TYPE:</label>

          <input type="checkbox" name="year"> Public

          <input type="checkbox" name="year"> Private
          
        </div>

        <div class="student-gender border">

          <label>GENDER: </label>
          
          <input type="checkbox" name="year" {{ ($student->gender == 'male') ? 'checked' : '' }}> Male

          <input type="checkbox" name="year" {{ ($student->gender == 'female') ? 'checked' : '' }}> Female

        </div>

        <div class="student-birth border">

          <label>BIRTHDAY:</label>

          {{ $student->birth_date }}
          
        </div>

        <div class="student-pic-border border less-right">
          <div class="image-pic">1x1</div>          
        </div>

        <!-- Next line -->

        <div class="student-age border">

          <label>AGE:</label>

          {{ $student->age }}
          
        </div>

        <div class="student-sem border">

          <label>SEMESTER:</label> 
          
          <input type="checkbox" name="year" {{ ($student->semester == '1st_semester') ? 'checked' : '' }}> 1ST SEM

          <input type="checkbox" name="year" {{ ($student->semester == '2nd_semester') ? 'checked' : '' }}> 2ND SEM
          
        </div>

        <div class="student-home-address border">

          <label>COMPLETE ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-place-birth border less-right">

          <label>PLACE OF BIRTH:</label>

          {{ $student->birth_place }}
          
        </div>

        <!-- Next line -->

        <div class="student-status border">

          <label>CIVIL STATUS:</label>

          {{ $student->civil_status }}
          
          
        </div>

        <div class="student-religion border">

          <label>Religion:</label>

          {{ $student->religion }}
          
        </div>

        <div class="student-nationality border">
          
          <label>NATIONALITY:</label>

          {{ $student->nationality }}

        </div>

        <div class="student-email border less-right">

          <label>EMAIL:</label>

          {{ $student->email }}
          
        </div>

        <!-- Next line -->

        <div class="student-school border">

          <label>JUNIOR HIGH SCHOOL:</label>

          {{ $student->previous_school }}
          
        </div>

        <div class="student-address border">

          <label>ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-contact border less-right">
          
          <label>CONTACT NUMBER:</label>

          {{ $student->phone_number }}

        </div>

        <!-- Next line -->

        <div class="student-subjects border">
          SUBJECTS
        </div>

        <div class="student-days border">
          DAYS
        </div>

        <div class="student-time border">
          TIME
        </div>

        <div class="student-room border">
          ROOM
        </div>

        <div class="student-tuition border">
          TUITION FEE
        </div>

        <div class="student-date-enrolled border">
          DATE ENROLLED
        </div>

        <div class="student-payment-details border less-right">
          PAYMENT DETAILS
        </div>

        <!-- Next line -->

        <div class="student-subjects-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-days-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
         
        </div>

        <div class="student-time-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-room-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-tuition-sub border">


          <div class="label-fee">Tuition</div>
          <div class="fee">11,500.00</div> 

          <div class="label-misce">Miscellaneous</div>
          <div class="fee-misce">2,850.00</div> 

          <div class="label-others">Others Fee</div>
          <div class="fee-others">3,150.00</div>

          <div class="fee-line">__________</div>
          <div class="fee-total">17,500.00</div>

        </div>

        <div class="student-date-enrolled-sub border">

          <div class="date-enrolled">
            {{ $date }}
          </div>

          <div class="border-line">Note:</div>

        </div>

        <div class="student-payment-details-sub border less-right">

          <div class="payment-qualified">
            Qualified Voucher
          </div>

          <div class="payment-recipient">Recipient (QVR)</div>

          <div class="payment-or">
            OR NO.
          </div>

          <div class="payment-date">
            Date
          </div>

          <div class="payment-amount">
            Amount
          </div>

        </div>

        <!-- Next line -->

        <div class="student-signature border custom-height custom-height">

          <label>STUDENT'S SIGNATURE:</label>

        </div>

        <div class="student-parent border custom-height custom-height">

          <label>NAME OF PARENT/GUARDIAN:</label>

          {{ $student->name_of_parent }}

        </div>

        <div class="student-contact border custom-height custom-height less-right">

          <label>CONTACT NO. OF PARENT/GUARDIAN:</label>

          {{ $student->parent_number }}

        </div>

        <!-- Next line -->

        <div class="student-registrar border custom-height custom-height less-bottom">

          <label>SCHOOL REGISTRAR:</label>

        </div>

        <div class="student-admin border custom-height custom-height less-bottom">

          <label>SCHOOL ADMINISTRATOR:</label>

        </div>

        <div class="student-staff border custom-height custom-height less-bottom less-right">

          <label>JPI ADMISSION STAFF:</label>

        </div>
        
      </div>

    </div>

    <div class="logo">
      <img src="/assets/images/default.png">
    </div>

  </div>

  <div class="padding-top">
    ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  </div>

   <!-- Cashier's copy -->
  <div class="school-brand">
    
    <div class="school-name">
      
      <p class="name">Japan-Philippines Institute of Technologies</p>

      @if(strtolower($student->campus) == "jpi san jose campus")

      <p class="text-muted">JAG bldg EVR Road purok 7 , Brgy San pedro, </p>
      <p class="text-muted">Area E. Sapang Palay  City of San Jose Del Monte</p>

      @endif

      @if(strtolower($student->campus) == "jpi plaridel campus")

      <p class="text-muted">Rocka commercial complex, Cagayan valley National Road,</p>
      <p class="text-muted">Tabang, Plaridel, Bulacan.</p>

      @endif

      @if(strtolower($student->campus) == "jpi santa maria campus")

      <p class="text-muted">3/F JM Squre Bldg. Poblacion, Santa Maria, Bulacan</p>

      @endif

      @if(strtolower($student->campus) == "jpi muzon campus")

      <p class="text-muted">M. Villarica Provincial road, zone 1, Muzon,</p>
      <p class="text-muted">city of san jose del monte, Bulacan</p>
      
      @endif

      <p class="copy-text">Cashier's Copy</p>

    </div>

    <div class="form-content">
      
      <div class="parent-content">

        <div class="form-title">
          SENIOR HIGH SCHOOL ENROLLMENT FORM S.Y __________
        </div>

        <div class="student-number border">

          <label>STUDENT NUMBER:</label> 
          
        </div>

        <div class="student-name border">
          
          <label>NAME (LAST, GIVEN, MIDDLE):</label> {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

        </div>

        <div class="student-level border">

          <label>YEAR LEVEL: </label>

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_11') ? 'checked' : '' }}> 11

          <input type="checkbox" name="year" {{ ($student->year_level == 'year_level_12') ? 'checked' : '' }}> 12
          
        </div>

        <div class="student-status-type border">
            
            <label>STATUS:</label>

            @if($student->enrollment_type == 'new_student')
              New Student
            @endif

            @if($student->enrollment_type == 'old_student')
              Old Student
            @endif

            @if($student->enrollment_type == 'transferee')
              Transferee
            @endif

        </div>

        <div class="student-pic border less-bottom less-right">
          
        </div>


        <!-- Next Line -->

        <div class="student-lrn border">

          <label>LRN:</label> {{ $student->lrn_number }}
          
        </div>

        <div class="student-track border">
          
          <label>TRACK:</label>

          {{ $student->strand }}

        </div>

        <div class="student-type border">

          <label>STUDENT TYPE:</label>

          <input type="checkbox" name="year"> Public

          <input type="checkbox" name="year"> Private
          
        </div>

        <div class="student-gender border">

          <label>GENDER: </label>
          
          <input type="checkbox" name="year" {{ ($student->gender == 'male') ? 'checked' : '' }}> Male

          <input type="checkbox" name="year" {{ ($student->gender == 'female') ? 'checked' : '' }}> Female

        </div>

        <div class="student-birth border">

          <label>BIRTHDAY:</label>

          {{ $student->birth_date }}
          
        </div>

        <div class="student-pic-border border less-right">
          <div class="image-pic">1x1</div>          
        </div>

        <!-- Next line -->

        <div class="student-age border">

          <label>AGE:</label>

          {{ $student->age }}
          
        </div>

        <div class="student-sem border">

          <label>SEMESTER:</label> 
          
          <input type="checkbox" name="year" {{ ($student->semester == '1st_semester') ? 'checked' : '' }}> 1ST SEM

          <input type="checkbox" name="year" {{ ($student->semester == '2nd_semester') ? 'checked' : '' }}> 2ND SEM
          
        </div>

        <div class="student-home-address border">

          <label>COMPLETE ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-place-birth border less-right">

          <label>PLACE OF BIRTH:</label>

          {{ $student->birth_place }}
          
        </div>

        <!-- Next line -->

        <div class="student-status border">

          <label>CIVIL STATUS:</label>

          {{ $student->civil_status }}
          
          
        </div>

        <div class="student-religion border">

          <label>Religion:</label>

          {{ $student->religion }}
          
        </div>

        <div class="student-nationality border">
          
          <label>NATIONALITY:</label>

          {{ $student->nationality }}

        </div>

        <div class="student-email border less-right">

          <label>EMAIL:</label>

          {{ $student->email }}
          
        </div>

        <!-- Next line -->

        <div class="student-school border">

          <label>JUNIOR HIGH SCHOOL:</label>

          {{ $student->previous_school }}
          
        </div>

        <div class="student-address border">

          <label>ADDRESS:</label>

          {{ $student->address }}
          
        </div>

        <div class="student-contact border less-right">
          
          <label>CONTACT NUMBER:</label>

          {{ $student->phone_number }}

        </div>

        <!-- Next line -->

        <div class="student-subjects border">
          SUBJECTS
        </div>

        <div class="student-days border">
          DAYS
        </div>

        <div class="student-time border">
          TIME
        </div>

        <div class="student-room border">
          ROOM
        </div>

        <div class="student-tuition border">
          TUITION FEE
        </div>

        <div class="student-date-enrolled border">
          DATE ENROLLED
        </div>

        <div class="student-payment-details border less-right">
          PAYMENT DETAILS
        </div>

        <!-- Next line -->

        <div class="student-subjects-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-days-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
         
        </div>

        <div class="student-time-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-room-sub border">

          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line"></div>
          <div class="line less-bottom"></div>
        
        </div>

        <div class="student-tuition-sub border">


          <div class="label-fee">Tuition</div>
          <div class="fee">11,500.00</div> 

          <div class="label-misce">Miscellaneous</div>
          <div class="fee-misce">2,850.00</div> 

          <div class="label-others">Others Fee</div>
          <div class="fee-others">3,150.00</div>

          <div class="fee-line">__________</div>
          <div class="fee-total">17,500.00</div>

        </div>

        <div class="student-date-enrolled-sub border">

          <div class="date-enrolled">
            {{ $date }}
          </div>

          <div class="border-line">Note:</div>

        </div>

        <div class="student-payment-details-sub border less-right">

          <div class="payment-qualified">
            Qualified Voucher
          </div>

          <div class="payment-recipient">Recipient (QVR)</div>

          <div class="payment-or">
            OR NO.
          </div>

          <div class="payment-date">
            Date
          </div>

          <div class="payment-amount">
            Amount
          </div>

        </div>

        <!-- Next line -->

        <div class="student-signature border custom-height custom-height">

          <label>STUDENT'S SIGNATURE:</label>

        </div>

        <div class="student-parent border custom-height custom-height">

          <label>NAME OF PARENT/GUARDIAN:</label>

          {{ $student->name_of_parent }}

        </div>

        <div class="student-contact border custom-height custom-height less-right">

          <label>CONTACT NO. OF PARENT/GUARDIAN:</label>

          {{ $student->parent_number }}

        </div>

        <!-- Next line -->

        <div class="student-registrar border custom-height custom-height less-bottom">

          <label>SCHOOL REGISTRAR:</label>

        </div>

        <div class="student-admin border custom-height custom-height less-bottom">

          <label>SCHOOL ADMINISTRATOR:</label>

        </div>

        <div class="student-staff border custom-height custom-height less-bottom less-right">

          <label>JPI ADMISSION STAFF:</label>

        </div>
        
      </div>

    </div>

    <div class="logo">
      <img src="/assets/images/default.png">
    </div>

  </div>

@endsection

@section('scripts')
	
	<script type="text/javascript" src="{{ asset('/assets/js/enrollment/function.js') }}"></script>

@stop