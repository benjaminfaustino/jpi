@extends('layouts.admin')

@section('title', 'Enrollment-list')

@section('stylesheets')

@section('content')

  <div class="content-wrapper">

  <!-- Get data from this route value /GetEnrollmentList-->
  <input id="get-value" type="hidden" value="EnrollmentList">
  <input type="hidden" :value="collections">

	<!-- Main content -->
    <section class="content hidden">

      <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
  <!--           <li class="active">
              <a href="#export" data-toggle="tab">
                Export Daily Status Report
              </a>
            </li> -->

            <li class="active">
              <a href="#summary" data-toggle="tab">
                Daily Summary Student <span id="over-all-count" class="pull-right label label-primary">0</span>
              </a>
            </li>

            <li class="">
              <a href="#export" data-toggle="tab">
                Export File
              </a>
            </li>

            <div class="date-picker-range">
                <div class="form-group">
                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" id="daterangepicker"/>
                    </div>
                </div>
            </div>

          </ul>
          <div class="tab-content">

            <!-- Tab Pane Export File -->
            <div class="tab-pane" id="export">

                <a href="/admin/export?start={{ $date_start }}&end={{$date_end}}">
                  <button class="btn btn-sm btn-success">
                    Export Report
                  </button>
                </a>
              
            </div>
            <!-- /.tab-pane Export file -->

            <!-- Tab Pane Daily Summary Count -->
            <div class="active tab-pane" id="summary">

              <div class="panel-group parent-sumary-count" id="sta_maria">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#sta_maria" href="#collapse_sta_maria">
                        Sta Mari Branch
                        <span class="pull-right label label-primary branch_count">0</span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse_sta_maria" class="panel-collapse collapse">
                    <div class="panel-body">

                      @include('/admin/export/summary/sta_maria_branch')

                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-group parent-sumary-count" id="san_jose">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#san_jose" href="#collapse_san_jose">
                        San Jose Branch
                        <span class="pull-right label label-primary branch_count">0</span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse_san_jose" class="panel-collapse collapse">
                    <div class="panel-body">

                      @include('/admin/export/summary/san_jose_branch')

                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-group parent-sumary-count" id="muzon">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#muzon" href="#collapse_muzon">
                        Muzon
   Branch                      <span class="pull-right label label-primary branch_count">0</span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse_muzon" class="panel-collapse collapse">
                    <div class="panel-body">

                      @include('/admin/export/summary/muzon_branch')

                    </div>
                  </div>
                </div>
              </div>

              <div class="panel-group parent-sumary-count" id="plaridel">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#plaridel" href="#collapse_plaridel">
                        Plaridel Branch
                        <span class="pull-right label label-primary branch_count">0</span>
                      </a>
                    </h4>
                  </div>
                  <div id="collapse_plaridel" class="panel-collapse collapse">
                    <div class="panel-body">

                      @include('/admin/export/summary/plaridel_branch')

                    </div>
                  </div>
                </div>
              </div>

            <!-- /.tab-pane Daily Summary Count -->
            </div>
          <!-- /.tab-content -->
          </div>
        <!-- /.nav-tabs-custom -->
        </div>
      </div>

  	</section>

@endsection

@section('scripts')
	
  <script type="text/javascript">

    $('#daterangepicker').daterangepicker({
      startDate: '{!! $date_start !!}', 
      endDate: '{!! $date_end !!}',
      locale: {
        format: 'MMMM/DD/YYYY'
      }
    });

    $(document).on('click' , '.applyBtn' , function(){
        

        var start = $('#daterangepicker').data('daterangepicker').startDate;
        var end = $('#daterangepicker').data('daterangepicker').endDate;

        start = start.format('D MMMM YYYY');
        end = end.format('D MMMM YYYY');

        var url = "/admin/reports/date?start="+ start +"&end="+ end +"";

        window.location = url;
    });

    $(document).on('click' , '.cancelBtn ' , function(){
        window.location = "/admin/reports";
    });
  </script>

  <script type="text/javascript" src="{{ asset('/assets/js/admin/function.js') }}"></script>

@stop