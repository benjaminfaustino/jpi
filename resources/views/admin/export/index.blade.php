<table>
    <thead>
    <tr>
        <!-- Student Data -->
        <th>LRN Number</th>
        <th>Name</th>
        <th>Gender</th>
        <th>Age</th>
        <th>Birth Date</th>
        <th>Phone Number</th>
        <th>Address</th>
        <th>Birth Place</th>
        <th>Civil Status</th>
        <th>Nationality</th>
        <th>Email</th>
        <th>Religion</th>
        <th>Parent's Name</th>

        <!-- School Data -->
        <th>Enrollment Type</th>
        <th>Previous School</th>
        <th>Year Level</th>
        <th>Semester</th>
        <th>Campus</th>
        <th>Strand</th>
        <th>Created_at</th>
    </tr>
    </thead>
    <tbody>
    @foreach($students_female as $student_female)
        <tr>
            <td>
                {{ $student_female->lrn_number }}
            </td>
            <td>
                {{ $student_female->last_name }}, {{ $student_female->first_name }} {{ $student_female->middle_name }}
            </td>
            <td>
                {{ $student_female->gender }}
            </td>
            <td>
                {{ $student_female->age }}
            </td>
            <td>
                {{ date('m-d-Y', strtotime($student_female->birth_date)) }}
            </td>
            <td>
                {{ $student_female->phone_number }}
            </td>
            <td>
                {{ $student_female->address }}
            </td>
            <td>
                {{ $student_female->birth_place }}
            </td>
            <td>
                {{ $student_female->civil_status }}
            </td>
            <td>
                {{ $student_female->nationality }}
            </td>
            <td>
                {{ $student_female->email }}
            </td>
            <td>
                {{ $student_female->religion }}
            </td>
            <td>
                {{ $student_female->name_of_parent }}
            </td>
            <td>

                @if($student_female->enrollment_type == "old_student")

                    Old Student

                @elseif($student_female->enrollment_type == "new_student")

                    New Student

                @else

                    Transferee

                @endif
            </td>
            <td>
                {{ $student_female->previous_school }}
            </td>
            <td>

                @if($student_female->year_level == "year_level_11")

                    Year Level 11

                @else

                    Year Level 12

                @endif
            </td>
            <td>

                @if($student_female->semester == "1st_semester")

                    First Semester

                @else

                    Second Semester

                @endif

            </td>
            <td>
                {{ $student_female->campus }}
            </td>
            <td>
                {{ $student_female->strand }}
            </td>
            <td>
                {{ $student_female->created_at }}
            </td>
        </tr>
    @endforeach

    @foreach($students_male as $student_male)
        <tr>
            <td>
                {{ $student_male->lrn_number }}
            </td>
            <td>
                {{ $student_male->last_name }}, {{ $student_male->first_name }} {{ $student_male->middle_name }}
            </td>
            <td>
                {{ $student_male->gender }}
            </td>
            <td>
                {{ $student_male->age }}
            </td>
            <td>
                {{ date('m-d-Y', strtotime($student_male->birth_date)) }}
            </td>
            <td>
                {{ $student_male->phone_number }}
            </td>
            <td>
                {{ $student_male->address }}
            </td>
            <td>
                {{ $student_male->birth_place }}
            </td>
            <td>
                {{ $student_male->civil_status }}
            </td>
            <td>
                {{ $student_male->nationality }}
            </td>
            <td>
                {{ $student_male->email }}
            </td>
            <td>
                {{ $student_male->religion }}
            </td>
            <td>
                {{ $student_male->name_of_parent }}
            </td>
            <td>

                @if($student_male->enrollment_type == "old_student")

                    Old Student

                @elseif($student_male->enrollment_type == "new_student")

                    New Student

                @else

                    Transferee

                @endif
            </td>
            <td>
                {{ $student_male->previous_school }}
            </td>
            <td>

                @if($student_male->year_level == "year_level_11")

                    Year Level 11

                @else

                    Year Level 12

                @endif
            </td>
            <td>

                @if($student_male->semester == "1st_semester")

                    First Semester

                @else

                    Second Semester

                @endif

            </td>
            <td>
                {{ $student_male->campus }}
            </td>
            <td>
                {{ $student_male->strand }}
            </td>
            <td>
                {{ $student_male->created_at }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>