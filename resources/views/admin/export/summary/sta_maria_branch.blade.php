<div class="panel-group first-parent-panel" id="sta_maria_grade_11">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#sta_maria_grade_11" href="#collapse_sta_maria_grade_11">
          Grade 11

          <span class="pull-right label label-warning first-parent-panel-count">0</span>
        </a>
      </h4>
    </div>
    <div id="collapse_sta_maria_grade_11" class="panel-collapse collapse">
      <div class="panel-body">

        <!-- Accordion Gas -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_11_Gas">
                  Gas

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_11_Gas" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "GAS" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                            {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "GAS" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion Gas -->

        <!-- Accordion AMB -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_11_ABM">
                  ABM

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_11_ABM" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "ABM" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "ABM" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion AMB -->

        <!-- Accordion TVL -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_11_TVL">
                  TVL

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_11_TVL" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "TVL" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "TVL" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion TVL -->

        <!-- Accordion HUMSS -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_11_HUMSS">
                  HUMSS

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_11_HUMSS" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "HUMSS" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "HUMSS" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion HUMSS -->

        <!-- Accordion STEM -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_11_STEM">
                  STEM

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_11_STEM" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "STEM" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_11" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "STEM" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion STEM -->

      </div>
    </div>
  </div>
</div>



<!-- Grade 12 -->


<div class="panel-group second-parent-panel" id="sta_maria_grade_12">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#sta_maria_grade_12" href="#collapse_sta_maria_grade_12">
          Grade 12

          <span class="pull-right label label-warning second-parent-panel-count">0</span>
        </a>
      </h4>
    </div>
    <div id="collapse_sta_maria_grade_12" class="panel-collapse collapse">
      <div class="panel-body">

        <!-- Accordion Gas -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_12_Gas">
                  Gas

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_12_Gas" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "GAS" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "GAS" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion Gas -->

        <!-- Accordion AMB -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_12_ABM">
                  ABM

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_12_ABM" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "ABM" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "ABM" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion AMB -->

        <!-- Accordion TVL -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_12_TVL">
                  TVL

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_12_TVL" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "TVL" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "TVL" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion TVL -->

        <!-- Accordion HUMSS -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_12_HUMSS">
                  HUMSS

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_12_HUMSS" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "HUMSS" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "HUMSS" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion HUMSS -->

        <!-- Accordion STEM -->
        <div class="panel-group parent-accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" href="#collapse_sta_maria_grade_12_STEM">
                  STEM

                  <span class="pull-right label label-success list-count">0</span>
                </a>
              </h4>
            </div>
            <div id="collapse_sta_maria_grade_12_STEM" class="panel-collapse collapse">

              <!-- Sub list -->
              <ul class="list-group">
                <!-- First Semester -->
                <li class="list-group-item first-layer">
                 First Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "STEM" && $student->semester == "1st_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- First Semester -->

                <!-- Second Semester -->
                <li class="list-group-item second-layer">
                 Second Semester
                  <!-- Sub list -->
                  <ul class="list-group">
                    <li class="list-group-item">
                      @foreach($students as $student)

                        @if($student->year_level == "year_level_12" && $student->campus == "JPI Santa Maria Campus" && $student->strand == "STEM" && $student->semester == "2nd_semester")

                          <li class="list-group-item count-layer">
                           {{ $student->last_name }}, {{ $student->first_name }} {{ $student->middle_name }}

                            <span class="pull-right">
                              {{ date("Y-m-d h:i:s a", strtotime($student->created_at)) }}
                            </span>
                          </li>

                        @endif

                      @endforeach
                    </li>
                  </ul>
                  <!-- Sub list -->
                </li>
                <!-- Second Semester -->

              </ul>
              <!-- Sub list -->

            </div>
          </div>
        </div>
        <!-- Accordion STEM -->

      </div>
    </div>
  </div>
</div>