
<meta property="og:image" content="{{ asset('assets/images/preview.png') }}">
<!DOCTYPE html>
<html lang="en">

 <head>
   @include('layouts.partials.header')
 </head>

<body>

	@yield('content')

	@include('layouts.partials.footer')

</body>
