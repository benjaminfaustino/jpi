<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Jpi-Tech @yield("title")</title>

    <!-- Bootstrap 3.3.7 -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/bootstrap.min.css') }}"> -->
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/font-awesome.min.css') }}"> -->
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/ionicons.min.css') }}"> -->
    <!-- jvectormap -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/jquery-jvectormap.css') }}"> -->
    <!-- Theme style -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/AdminLTE.min.css') }}"> -->

    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/jquery-ui.css') }}"> -->

    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <!-- <link rel="stylesheet" href="{{ asset('/assets/css/plugins/all-skins.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/assets/css/plugins/dataTables.bootstrap.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/assets/css/plugins/select2.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/assets/css/datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/custom.css') }}"> -->

    <link rel="stylesheet" href="{{ asset('/assets/dist/css/vendor.min.css') }}">
    
    @yield("stylesheets")

    <!-- Overide CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/css/global.css') }}">

    <!-- <link rel="icon" href="{{ asset('/images/headers/favicon.ico') }}"> -->

</head>
<body id="body" class="skin-black sidebar-mini">
    <!-- Content -->
    <div id="app" class="wrapper">

        <div class="loading">
            Loading
        </div>

        @if(Auth::check())

            @include('layouts/partials/admin/header')

            @include('layouts/partials/admin/left-sidebar')

            @yield("content")

        @endif

    </div>

    <!-- jQuery 3 -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery.min.js') }}"></script> -->
    <!-- Bootstrap 3.3.7 -->
<!--     <script type="text/javascript" src="{{ asset('/assets/js/plugins/bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/plugins/vue.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/plugins/axios.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/app.js') }}"></script> -->

    <!-- <script type="text/javascript" src="{{ asset('/assets/js/global.js') }}"></script> -->

<!--     <script type="text/javascript" src="{{ asset('/assets/js/plugins/select2.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery-ui.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/assets/js/plugins/moment.js') }}"></script> -->

    <!-- FastClick -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/fastclick.js') }}"></script> -->
    <!-- AdminLTE App -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/adminlte.min.js') }}"></script> -->
    <!-- Sparkline -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery.sparkline.min.js') }}"></script> -->
    <!-- jvectormap  -->
<!--     <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery-jvectormap-world-mill-en.js') }}"></script> -->
    <!-- SlimScroll -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/jquery.slimscroll.min.js') }}"></script> -->
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/dashboard2.js') }}"></script> -->
    <!-- AdminLTE for demo purposes -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/plugins/demo.js') }}"></script> -->

    <script type="text/javascript" src="{{ asset('/assets/dist/js/vendor.min.js') }}"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        axios.create({
          baseURL: 'https://some-domain.com/api/',
          timeout: 1000,
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
        });
    </script>


    @yield("scripts")

    <!-- Overide JS -->
    <!-- <script type="text/javascript" src="{{ asset('/assets/js/global.js') }}"></script> -->
    
</body>
</html>
