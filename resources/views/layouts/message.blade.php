@if ($errors->any())

	@foreach ($errors->all() as $error)

		@if($error == 'You Are Already Enrolled! Please Contact School Admin For More Details.')

			<div class="alert alert-success">
			    <ul class="mb-0">
			        <li>{{ $error }}</li>
			    </ul>
			</div>

			@break

		@else

			@include('layouts.error')

			@break

		@endif

	@endforeach

@endif