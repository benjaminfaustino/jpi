<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<title>Jpi Tech - @yield('title')</title>
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="{{ asset('assets/dist/css/app.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/vendor.css') }}">

<!-- Custom styles for this template -->
@yield('style')