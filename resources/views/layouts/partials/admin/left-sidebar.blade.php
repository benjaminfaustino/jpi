<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('/assets/images/default.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
<!--       <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
<!--         <li id="dashboard" class="nav-list">
          <a href="{{ url('admin-panel') }}">
            <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.html"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
            <li class="active"><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
          </ul>
        </li> -->

        <li id="member" class="nav-list {{ Request::path() ==  'admin/enrollment-list' ? 'active' : ''  }}">
          <a href="{{ url('/admin/enrollment-list') }}">
            <i class="fas fa-users"></i> <span>Enrolled Students</span>
          </a>
        </li>

        <li id="sta-maria" class="nav-list {{ Request::path() ==  'admin/enrollment-list/sta_maria' ? 'active' : ''  }}">
          <a href="{{ url('/admin/enrollment-list/sta_maria') }}">
            <i class="fas fa-users"></i> <span>Sta Maria Branch</span>
          </a>
        </li>

        <li id="san-jose" class="nav-list {{ Request::path() ==  'admin/enrollment-list/san_jose' ? 'active' : ''  }}">
          <a href="{{ url('/admin/enrollment-list/san_jose') }}">
            <i class="fas fa-users"></i> <span>San Jose Branch</span>
          </a>
        </li>

        <li id="sta-maria" class="nav-list {{ Request::path() ==  'admin/enrollment-list/muzon' ? 'active' : ''  }}">
          <a href="{{ url('/admin/enrollment-list/muzon') }}">
            <i class="fas fa-users"></i> <span>Muzon Branch</span>
          </a>
        </li>

        <li id="sta-maria" class="nav-list {{ Request::path() ==  'admin/enrollment-list/plaridel' ? 'active' : ''  }}">
          <a href="{{ url('/admin/enrollment-list/plaridel') }}">
            <i class="fas fa-users"></i> <span>Plaridel Branch</span>
          </a>
        </li>

        <li id="sta-maria" class="nav-list {{ Request::path() ==  'admin/reports' ? 'active' : ''  }}">
          <a href="{{ url('/admin/reports') }}">
            <i class="fa fa-file"></i> <span>Reports</span>
          </a>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
