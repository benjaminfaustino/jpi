<!-- Bootstrap core JavaScript
================================================= -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{ asset('assets/dist/js/app.min.js') }}"></script>

@yield('script')