@extends('layouts.vendor')

@section('title', 'Congratulations')

@section('style')

@endsection

@section('content')

<div class="header-hero"></div>

<div class="container register">
	
	<div class="row">
		<div class="col-md-3 register-left">
			<!-- <img src="https://image.ibb.co/n7oTvU/logo_white.png" alt=""/> -->
			<h3>Welcome</h3>
			<p>This is JPI Technologies Enrollment Form. Please see the instruction</p>
			<a href="/enrollment-form">
				<input type="submit" name="" value="Enroll Now"/><br/>
			</a>
		</div>
		<div class="col-md-9 register-right">
			<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
<!-- 				<li class="nav-item">
					<a class="nav-link active" id="instruction-tab" data-toggle="tab" href="#instruction" role="tab" aria-controls="instruction" aria-selected="true">Instruction</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="enrollment-form-tab" data-toggle="tab" href="#enrollment-form" role="tab" aria-controls="enrollment-form" aria-selected="false">Form</a>
				</li> -->
			</ul>
			<div class="tab-content" id="myTabContent">
				<div class="tab-pane fade show active" id="instruction" role="tabpanel" aria-labelledby="instruction-tab">
					<h3  class="register-heading">Instructions</h3>
					<div class="row register-form">

						<p class="reminder">
							
							@include('flash::message')

						</p>

					</div>
				</div>
		</div>
	</div>

</div>

@endsection


@section('script')

@endsection