<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnrollmentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollment_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('enrollment_type')->nullable();
            $table->text('first_name')->nullable();
            $table->text('middle_name')->nullable();
            $table->text('last_name')->nullable();
            $table->text('birth_date')->nullable();
            $table->bigInteger('age')->nullable();
            $table->text('religion')->nullable();
            $table->text('name_of_parent')->nullable();
            $table->text('previous_school')->nullable();
            $table->text('year_level')->nullable();
            $table->text('semester')->nullable();
            $table->text('campus')->nullable();
            $table->text('gender')->nullable();
            $table->text('email')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->text('address')->nullable();
            $table->text('birth_place')->nullable();
            $table->text('civil_status')->nullable();
            $table->text('nationality')->nullable();
            $table->bigInteger('parent_number')->nullable();
            $table->text('lrn_number')->nullable();
            $table->text('question')->nullable();
            $table->text('strand')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enrollment_users');
    }
}
