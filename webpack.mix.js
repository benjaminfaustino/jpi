const mix = require('laravel-mix');

/**
 * application code / app assets
 */
mix.styles([
    'public/assets/css/bootstrap.min.css',
    'public/assets/css/datepicker.css',
    'public/assets/css/fontawesome.min.css',
    'public/assets/css/custom.css',
], 'public/assets/dist/css/app.min.css');

mix.scripts([
    'public/assets/js/jquery.min.js',
    'public/assets/js/tether.min.js',
    'public/assets/js/moment.min.js',
    'public/assets/js/bootstrap.min.js',
    'public/assets/js/datepicker.js',
    'public/assets/js/vendor/jquery.ui.widget.js',
    'public/assets/js/jquery.iframe-transport.js',
    'public/assets/js/jquery.fileupload.js',
    'public/assets/js/custom.js',
], 'public/assets/dist/js/app.min.js');

mix.styles([
    'public/assets/css/plugins/bootstrap.min.css',
    'public/assets/css/plugins/font-awesome.min.css',
    'public/assets/css/plugins/ionicons.min.css',
    'public/assets/css/plugins/jquery-jvectormap.css',
    'public/assets/css/plugins/AdminLTE.min.css',
    'public/assets/css/plugins/jquery-ui.css',
    'public/assets/css/plugins/all-skins.min.css',
    'public/assets/css/plugins/dataTables.bootstrap.min.css',
    'public/assets/css/plugins/select2.min.css',
    'public/assets/css/datepicker.css',
    'public/assets/css/plugins/daterangepicker.css',
    'public/assets/css/custom.css'
    // 'public/assets/css/global.css',
], 'public/assets/dist/css/vendor.min.css');

mix.scripts([
    'public/assets/js/plugins/jquery.min.js',
    'public/assets/js/plugins/bootstrap.min.js',
    'public/assets/js/plugins/vue.js',
    'public/assets/js/plugins/axios.min.js',
    'public/assets/js/plugins/app.js',
    'public/assets/js/plugins/select2.min.js',
    'public/assets/js/plugins/jquery-ui.js',
    'public/assets/js/plugins/moment.js',
    'public/assets/js/plugins/daterangepicker.js',
    'public/assets/js/plugins/fastclick.js',
    'public/assets/js/plugins/adminlte.min.js',
    'public/assets/js/plugins/jquery.sparkline.min.js',
    'public/assets/js/plugins/jquery-jvectormap-1.2.2.min.js',
    'public/assets/js/plugins/jquery-jvectormap-world-mill-en.js',
    'public/assets/js/plugins/jquery.slimscroll.min.js',
    'public/assets/js/plugins/demo.js',
], 'public/assets/dist/js/vendor.min.js');
