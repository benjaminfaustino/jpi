<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EnrollmentUser extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $table = 'enrollment_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'enrollment_type',
		'first_name',
		'middle_name',
		'last_name',
		'birth_date',
		'age',
		'religion',
		'name_of_parent',
		'previous_school',
		'year_level',
		'semester',
		'campus',
		'gender',
		'email',
		'phone_number',
		'address',
		'birth_place',
		'civil_status',
		'nationality',
		'parent_number',
		'lrn_number',
		'question',
		'strand',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function documents()
    {
       return $this->hasMany('App\Models\Document', 'enrollment_user_id');
    }

    public function dates()
    {
       return $this->hasMany('App\Models\EnrollmentDates', 'student_id');
    }
}
