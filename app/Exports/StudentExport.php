<?php

namespace App\Exports;

use App\Models\EnrollmentUser;
// use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use App\Exports\Sheets\InvoicesPerMonthSheet;

class StudentExport implements WithMultipleSheets
{
    /**
     * @return array
     */
    public function sheets(): array
    {
        $sheets = [];

        $branches = [ 'Santa Maria', 'San Jose', 'Muzon', 'Plaridel' ];

        foreach ($branches as $key => $value) {
            $sheets[] = new InvoicesPerMonthSheet($value);
        }

        return $sheets;
    }
}
