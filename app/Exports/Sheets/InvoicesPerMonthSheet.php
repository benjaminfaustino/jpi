<?php

namespace App\Exports\Sheets;

use App\Models\EnrollmentUser;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromView;

class InvoicesPerMonthSheet implements FromView, WithTitle
{
    private $branch_name;

    public function __construct(string $branch_name)
    {
        $this->branch_name = $branch_name;
    }

    public function view(): View
    {
        date_default_timezone_set('Asia/Manila');

        $m = date("m");
        $d = date("d");
        $y = date("Y");
        $previous_date               = date("d",strtotime("-1 days"));
        $previous_two_date           = date("d",strtotime("-2 days"));
        $to_today_cut_off_date       = date("Y-m-d", mktime(16,30,0, $m, $d, $y));
        $from_yesterday_cut_off_date = date("Y-m-d", mktime(16,31,0, $m, $previous_date, $y));
        $from_previous_two_date      = date("Y-m-d", mktime(16,31,0, $m, $previous_two_date, $y));

        // $students_male = EnrollmentUser::whereBetween('created_at', [$from_yesterday_cut_off_date." 16:31:00", $to_today_cut_off_date." 16:30:00"])->where('campus', 'JPI ' . $this->branch_name . ' Campus')->orderBy('last_name', 'asc')->where('gender', 'male')->get();

        // whereBetween('created_at', ["2010-01-01 00:00:00", $from_previous_two_date." 16:30:00"])

        $students_male = EnrollmentUser::where('campus', 'JPI ' . $this->branch_name . ' Campus')->orderBy('last_name', 'asc')->where('gender', 'male')->get();

        $students_female = EnrollmentUser::where('campus', 'JPI ' . $this->branch_name . ' Campus')->orderBy('last_name', 'asc')->where('gender', 'female')->get();

        return view('admin.export.index', [
            'students_male' => $students_male,
            'students_female' => $students_female
        ]);
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->branch_name;
    }
}