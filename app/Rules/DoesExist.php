<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\EnrollmentDates;

class DoesExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $data = EnrollmentDates::where('lrn_number', $value)->where('school_year', env('SCHOOL_YEAR'))->first();
        
        if(empty($data)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "You're Already Enrolled For This School Year. Please Contact School Adiministrator For Assistance.";
    }
}
