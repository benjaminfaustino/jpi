<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\EnrollmentUser;

class DoesNotExist implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $data = EnrollmentUser::where('lrn_number', $value)->first();
        
        if(!empty($data)) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'LRN Number Not Found. Please contact School Administrator.';
    }
}
