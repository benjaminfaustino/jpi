<?php 

namespace App\Requests\Enrollment;

use App\Requests\GlobalRequest as Request;
use App\Rules\DoesNotExist;
use App\Rules\DoesExist;

class UpdateOldEnrollmentRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'lrn_number' => ['required', New DoesNotExist, new DoesExist],
			'year_level' => 'required',
			'semester' => 'required',
		];
	}

	public function messages()
	{
		return [];
	}

}
