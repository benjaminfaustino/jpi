<?php 

namespace App\Requests\Enrollment;

use App\Requests\GlobalRequest as Request;

class UpdateEnrollmentRequest extends Request {

	/**
	 * Determine if the project is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name' => 'required',
			'last_name' => 'required',
			'enrollment_type' => 'required',
			// 'email'  => 'unique:enrollment_users,email',
			'birth_date' => 'required',
			'age' => 'required',
			'name_of_parent' => 'required',
			'previous_school' => 'required',
			'year_level' => 'required',
			'semester' => 'required',
			'campus' => 'required',
			// 'image' => 'required',
			'gender' => 'required',
			'phone_number' => 'required',
			'address' => 'required',
			'civil_status' => 'required',
			'nationality' => 'required',
			'parent_number' => 'required',
			// 'lrn_number' => 'required',
			'strand' => 'required',
			// 'documents' => 'required',
		];
	}

	public function messages()
	{
		return [
			'image.required' => 'Please attach your 2x2 picture!',
			'documents.required' => 'Please attach the requirement files! (At least Birth Certificate)',
		];
	}


}
