<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);

         Validator::extend('uniqueFirstAndLastName', function ($attribute, $value, $parameters, $validator) {
            $count = \DB::table('enrollment_users')->where('first_name', $value)
                                        ->where('last_name', $parameters[0])
                                        ->count();

            return $count === 0;
        });
    }
}
