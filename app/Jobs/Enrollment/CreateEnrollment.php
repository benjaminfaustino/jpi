<?php 

namespace App\Jobs\Enrollment;

use EnrollmentUser, Document, Batch;
use App\Jobs\Job;

use MultipleUploader;

class CreateEnrollment extends Job {

	public $content;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct($content) {

		$this->content = $content;
	}

	public function handle() {

		$data = EnrollmentUser::create($this->content);

		$multipleUploader = new MultipleUploader;

		$columns = [
	     'file_name',
	     'type',
	     'uuid',
	     'enrollment_user_id',
		];

		$instance = new Document;

		if (isset($this->content['grades'])) {
			$grades = $multipleUploader->upload($this->content['grades'], $data->id, 'uploads/grades/', 'grades');

			if(!empty($grades)) Batch::insert($instance, $columns, $grades);
		}

		if (isset($this->content['documents'])) {
			$documents = $multipleUploader->upload($this->content['documents'], $data->id, 'uploads/documents/', 'documents');

			if(!empty($documents)) Batch::insert($instance, $columns, $documents);
		}

		if (isset($this->content['image'])) {
			$image = $multipleUploader->upload($this->content['image'], $data->id, 'uploads/images/', 'image');

			if(!empty($image)) Batch::insert($instance, $columns, $image);
		}

		return true;

	}

}
