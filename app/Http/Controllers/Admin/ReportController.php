<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use View, Response;
use App\Exports\StudentExport;
use App\Models\EnrollmentUser;
use Maatwebsite\Excel\Facades\Excel;

use Controller as Controller;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        date_default_timezone_set('Asia/Manila');

        if($request->get('start')) {

            $start = $request->get('start');
            $end = $request->get('end'); 

            $start_date = date('Y-m-d', strtotime($start));
            $end_date = date('Y-m-d', strtotime($end));

        } else {

            $m = date("m");
            $d = date("d");
            $y = date("Y");

            $previous_date  = date("d",strtotime("-1 days"));
            $start_date     = date("Y-m-d", mktime(16,31,0, $m, $previous_date, $y));
            $end_date       = date("Y-m-d", mktime(16,30,0, $m, $d, $y));

        }

        $students = EnrollmentUser::whereBetween('created_at', [$start_date." 16:31:00", $end_date." 16:30:00"])->get();

        $date_start = date("F/d/Y", strtotime($start_date));
        $date_end = date("F/d/Y", strtotime($end_date));

        return view::make('admin/report/index', compact('students','date_start', 'date_end'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function export(Request $request) 
    {
        date_default_timezone_set('Asia/Manila');

        $date = date("M-d-Y");
        $time = date("h:i:sa");

        return Excel::download(new StudentExport(), 'Daily Status Report - '. $date .' - '.$time .'.xlsx');
    }
}
