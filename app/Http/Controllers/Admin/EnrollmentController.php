<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use EnrollmentUser, Input, Batch, File, Document, EnrollmentDates;

use View, Response;
use CreateEnrollment, UpdateEnrollment;
use CreateEnrollmentRequest, UpdateEnrollmentRequest, UpdateOldEnrollmentRequest;
use Laracasts\Flash\Flash;
use Carbon\Carbon;

use Controller as Controller;

class EnrollmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = 'enrollment')
    {
        return view::make('admin/'.$type.'/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEnrollmentRequest $request)
    {
        $this->dispatch(new CreateEnrollment( $request->all()));

        if($request['enrollment_type'] == 'transferee' || $request['lrn_number'] == '')
        {
            $text = "Your data is for evaluation/verification. We will contact you after this process.";
        }
        else
        {
            $text = "You are officially enrolled!";
        }

        Flash::success($text);

        return redirect()->route('enrollment_form');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEnrollmentRequest $request, $id)
    {
        $data = $this->dispatch(new UpdateEnrollment( $request->all(), $request['id'] ) );

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        EnrollmentUser::find($id)->delete();
    }

    public function downloadFile($id) {

        $document = Document::find($id);

        $download = public_path().'/uploads/images/' . $document->file_name;

        return response()->download($download);

    }

    public function getEnrollmentList()
    {
        $data = EnrollmentUser::with('documents', 'dates')->orderBy('last_name', 'asc')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function getMuzonList()
    {
        $data = EnrollmentUser::with('documents', 'dates')->where('campus', 'JPI Muzon Campus')->orderBy('last_name', 'asc')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function getPlaridelList()
    {
        $data = EnrollmentUser::with('documents', 'dates')->where('campus', 'JPI Plaridel Campus')->orderBy('last_name', 'asc')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function getSanJoseList()
    {
        $data = EnrollmentUser::with('documents', 'dates')->where('campus', 'JPI San Jose Campus')->orderBy('last_name', 'asc')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }

    public function getStaMariaList()
    {
        $data = EnrollmentUser::with('documents', 'dates')->where('campus', 'JPI Santa Maria Campus')->orderBy('last_name', 'asc')->get();

        return Response::json(
            [   
                'data' => $data,
            ]
        );
    }


    public function instruction()
    {
        return view::make('enrollments/instruction');
    }

    public function enroll()
    {
        return view::make('enrollments/enroll');
    }

    public function enrollOld()
    {
        return view::make('enrollments/enroll_old');
    }

    public function bulk(Request $request)
    {
        Batch::update('Enrollments', $request->all(), 'id');
    }

    public function print($id)
    {
        $student = EnrollmentUser::find($id);
        $findDate    = EnrollmentDates::where('lrn_number', $student->lrn_number)->latest('created_at')->first();

        $date = null;
        if ($findDate) {
            $date = $findDate->created_at->toDateTimeString();
            $date = date("d/m/Y", strtotime($date));
        } else {
            $date = $student->created_at->toDateTimeString();
            $date = date("d/m/Y", strtotime($date));
        }

        $subjects = array(
            "year_level_11" => 
            [

                "1st_semester" => 
                [

                    "subjects" => 
                    [
                        "Oral Communication in Context",
                        "Komunikasyon at Pananaliksik sa Wikang Filipino at Kulturang Pilipino",
                        "General Mathematics",
                        "Earth and Life Science",
                        "Physical Education and Health",
                        "English for Academic and Professional Purposes"
                    ],

                    "ABM" => 
                    [
                        "Business Mathematics",
                        "Organization and Management"
                    ],

                    "STEM" => 
                    [
                        "Pre-Calculus",
                        "General Biology 1"
                    ],

                    "HUMSS" => 
                    [
                        "Disciplines and Ideas in the Social Sciences"
                    ],

                    "GAS" => 
                    [
                        "Organization and Management",
                        "Elective 1"
                    ],

                    "TVL" => 
                    [
                        "Computer System Servicing (NC II)",
                    ]

                ],

                "2nd_semester" => 
                [

                    "subjects" => 
                    [
                        "Reading and Writing Skills",
                        "Pagbasa at Pagsusuri ng Iba’t ibang Teksto Tungo sa Pananaliksik",
                        "Statistics and Probability",
                        "Physical Science",
                        "Physical Education and Health",
                        "Filipino sa Piling Larangan (Akademik)",
                        "Practical Research 1"
                    ],

                    "ABM" => 
                    [
                        "Fundamentals of Accountancy, Business and Management 1",
                        "Principles of Marketing"
                    ],

                    "STEM" => 
                    [
                        "Basic Calculus",
                        "General Biology 2"
                    ],

                    "HUMSS" => 
                    [
                        "Disciplines and Ideas in the Applied Social Sciences",
                        "Creative Writing/Malikhaing Pagsulat"
                    ],

                    "GAS" => 
                    [
                        "Disaster Readiness and Risk Reduction",
                        "HUMSS 1:Creative Writing/Malikhaing Pagsulat"
                    ],

                    "TVL" => 
                    [
                        "Computer System Servicing (NC II)"
                    ]

                ]

            ],
            "year_level_12" => 
            [
                "1st_semester" => 
                [

                    "subjects" => 
                    [
                        "21st Century Literature from the Philippines and the World",
                        "Introduction to the Philosophy of the Human Person",
                        "Understanding Culture, Society and Politics",
                        "Contemporary Philippine Arts from the Regions",
                        "Physical Education and Health"
                    ],

                    "ABM" => 
                    [
                        "Japanese language",
                        "Entrepreneurship",
                        "Practical Research 2",
                        "Fundamentals of Accountancy, Business and Management 2"
                    ],

                    "STEM" => 
                    [
                        "Entrepreneurship",
                        "Practical Research 2",
                        "General Physics 1",
                        "General Chemistry 1"
                    ],

                    "HUMSS" => 
                    [
                        "Entrepreneurship",
                        "Practical Research 2",
                        "Introduction to World Religions and Belief Systems",
                        "Philippine Politics and Governance",
                        "Creative Nonfiction"
                    ],

                    "GAS" => 
                    [
                        "Entrepreneurship",
                        "Practical Research 2",
                        "HUMSS 2: Introduction to World Religions and Belief Systems",
                        "Philippine Politics and Governance"
                    ],

                    "TVL" => 
                    [
                        "Japanese language",
                        "English for Academic and Professional Purposes",
                        "Computer System Servicing (NC II)"
                    ]

                ],

                "2nd_semester" => 
                [

                    "subjects" => 
                    [
                        "Media and Information Literacy",
                        "Personal Development",
                        "Physical Education and Health",
                    ],

                    "ABM" => 
                    [
                        "Empowerment Technologies (for the Strand)",
                        "Inquiries, Investigations and Immersion",
                        "Applied Economics",
                        "Business Ethics and Social Responsibility",
                        "Business Finance",
                        "Business Enterprise Simulation"
                    ],

                    "STEM" => 
                    [
                        "Contemporary Philippine Arts from the Regions",
                        "Empowerment Technologies (for the Strand)",
                        "Inquiries, Investigations and Immersion",
                        "General Physics 2",
                        "General Chemistry 2",
                        "Research/Capstone Project"
                    ],

                    "HUMSS" => 
                    [
                        "Empowerment Technologies (for the Strand)",
                        "Inquiries, Investigations and Immersion",
                        "Trends, Networks, and Critical Thinking in the 21st Century",
                        "Community Engagement, Solidarity, and Citizenship (CSC)",
                        "Culminating Activity"
                    ],

                    "GAS" => 
                    [
                        "Empowerment Technologies (for the Strand)",
                        "Inquiries, Investigations and Immersion",
                        "Applied Economics",
                        "Elective 2 (from any Track/Strand)",
                        "Work Immersion/Culminating Activity"
                    ],

                    "TVL" => 
                    [   
                        "Work Immersion",
                        "Empowerment Technologies (for the Strand)",
                        "Inquiries, Investigations and Immersion",
                        "Computer System Servicing (NC II)"
                    ]

                ]
            ]
        );

        return view::make('admin/print/index', compact('student', 'subjects', 'date'));
    }

    public function UpdatePrintStatus($id){

        $find = EnrollmentUser::find($id);

        $status = ($find->print_status) ? '' : 1;

        $find->print_status = $status;

        $find->update();

        return Response::json(
            [   
                'print_status' => $status,
            ]
        );

    }

    public function updateOld(UpdateOldEnrollmentRequest $request){

        $lrn_number = $request->lrn_number;
        $year_level = $request->year_level;
        $semester   = $request->semester;

        $student = EnrollmentUser::where('lrn_number', $lrn_number)->first();

        $student->year_level   = $year_level;
        $student->semester     = $semester;
        $student->print_status = '';
        $student->enrollment_type = "old_students";

        $student->update();

        EnrollmentDates::create(['school_year' => env('SCHOOL_YEAR'), 'lrn_number' => $lrn_number, 'student_id' => $student->id]);

        Flash::success("You are officially enrolled!");

        return redirect()->route('enrollment_form_old');

    }

    public function deleteEnrolledDate($id){

        EnrollmentDates::find($id)->delete();

    }
}
