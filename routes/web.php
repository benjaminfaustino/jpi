<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/instructions');
});

// Client Side
Route::get('/instructions', 'Admin\EnrollmentController@instruction')->name('instructions');
Route::get('/enrollment-form', 'Admin\EnrollmentController@enroll')->name('enrollment_form');
Route::get('/enrollment-form-old', 'Admin\EnrollmentController@enrollOld')->name('enrollment_form_old');
Route::post('/enrollments/store', 'Admin\EnrollmentController@store')->name('enrollment.store');
Route::post('/enrollments/update-old', 'Admin\EnrollmentController@updateOld')->name('enrollment.update_old');


// Admin Side
Route::group(['middleware' => ['auth'], 'prefix' => 'admin'], function () {

	// Enrollment Show UI Dynamically
	Route::get('/enrollment-list/{type}', 'Admin\EnrollmentController@index')->name('enrollment.index');

	// Enrollment Resource
	// Enrollment Update
	Route::post('/enrollment-list/put/{id}', 'Admin\EnrollmentController@update');
	Route::resources([
		'enrollment-list' => 'Admin\EnrollmentController'
	]);

	// Print Student Info UI
	Route::get('/print/{id}', 'Admin\EnrollmentController@print');

	// Get School List
	Route::post('/enrollment-list/getEnrollmentList', 'Admin\EnrollmentController@getEnrollmentList');
	Route::post('/enrollment-list/getMuzonList', 'Admin\EnrollmentController@getMuzonList');
	Route::post('/enrollment-list/getPlaridelList', 'Admin\EnrollmentController@getPlaridelList');
	Route::post('/enrollment-list/getSanJoseList', 'Admin\EnrollmentController@getSanJoseList');
	Route::post('/enrollment-list/getStaMariaList', 'Admin\EnrollmentController@getStaMariaList');

	// Report UI
	Route::get('/reports', 'Admin\ReportController@index');
	Route::get('/reports/{date}', 'Admin\ReportController@index');

	// Download Excel File
	Route::get('/export', 'Admin\ReportController@export');

	// Update Print Status
	Route::get('/update-print-status/{id}', 'Admin\EnrollmentController@UpdatePrintStatus');

	// Download File
	Route::get('download/{id}', 'DownloadController@downloadFile');

	Route::post('/delete-enrollment-date/{id}', 'Admin\EnrollmentController@deleteEnrolledDate');

});


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
