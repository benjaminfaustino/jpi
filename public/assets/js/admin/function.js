var first_total    = 0;
var second_total   = 0;
var over_all_count = 0;

(function($) {

	$("section.content").removeClass("hidden");
	$(".loader").addClass("hidden");

	$(".loading").addClass("hidden");

	parentSummaryCount();

}(jQuery));

function parentSummaryCount(){
	
	$(".parent-sumary-count").each(function(){

		first_total  = 0;
		second_total = 0;

		firstParentPanelCount( $(this) );
		secondParentPanelCount( $(this) );

		countOverAllTotal( $(this) );

	});
}

var first_semester_total = 0;

function firstParentPanelCount(el) {

	el.find(".first-parent-panel").each(function(){

		first_semester_total = 0;

		countFirstLayer( $(this), first_semester_total );

	});

}

var second_semester_total = 0;

function secondParentPanelCount(el) {

	el.find(".second-parent-panel").each(function(){

		second_semester_total = 0;

		countSecondLayer( $(this), second_semester_total );

	});

}

function countFirstLayer(el, first_semester_total){

	el.find(".first-layer").each(function(){

		var find_li        = $(this).find(".count-layer");
		var count_list     = find_li.length;

		first_semester_total = first_semester_total + count_list;

		first_total = first_semester_total;

		addNumericNumber(find_li);

		$(this).parents(".parent-accordion").find(".list-count").html(count_list);
		$(this).parents(".first-parent-panel").find(".first-parent-panel-count").html(first_semester_total);

	});

}

function countSecondLayer(el, second_semester_total,  second_strand_total){

	el.find(".first-layer").each(function(){

		var find_li        = $(this).find(".count-layer");
		var count_list     = find_li.length;

		second_semester_total = second_semester_total + count_list;
		second_strand_total   = second_strand_total + second_semester_total;

		second_total = second_semester_total;

		addNumericNumber(find_li);

		$(this).parents(".parent-accordion").find(".list-count").html(count_list);
		$(this).parents(".second-parent-panel").find(".second-parent-panel-count").html(second_semester_total);

	});

}

function addNumericNumber(find_li){

	find_li.each(function(index){

		var html = $(this).html();
		count = index + 1;
		html  = count + ". " + html;

		$(this).html(html);

	});

}

function countOverAllTotal(el){

	over_all_count = over_all_count + first_total + second_total;

	$("#over-all-count").html(over_all_count);

	el.find(".branch_count").html( first_total + second_total );

}