$(function(){

    $('#birthday').datepicker({
        format: 'mm/dd/yyyy',
    });

    $('.prev i').removeClass();
    $('.prev i').addClass("fas fa-arrow-left");

    $('.next i').removeClass();
    $('.next i').addClass("fas fa-arrow-right");

	$("#fileupload_grades").change(function(){

		imagesPreview(this, 'div.list-grades');

	});

	$("#fileupload_documents").change(function(){

		imagesPreview(this, 'div.list-documents');

	});

	$("#fileupload_image").change(function(){

		imagesPreview(this, 'div.list-image');

	});


    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(e) {

				$("<span class=\"parent\">" +
	            "<img class=\"imageThumb\" src=\"" + e.target.result + "\"/>" +
	            "<br/><button class=\"remove btn btn-xs btn-success\">Delete</button>" +
	            "</span>").appendTo(placeToInsertImagePreview);

				$(".remove").click(function(){
					$(this).parent(".parent").remove();
				});

                    // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };

    $("#year_level").change(function(){ 
        $("#year_level_12").addClass("hidden");
        $("#year_level_11").addClass("hidden");

        $("#" + $(this).val()).removeClass("hidden");
    });

    $("#strand").change(function(){ 
        $(".subject-target").addClass("hidden");

        $("." + $(this).val()).each(function(index, el){

            $(el).removeClass("hidden");

        });
    });

    $("#semester").change(function(){ 
        $(".semester-target").addClass("hidden");

        $("#" + $(this).val()).removeClass("hidden");
    });

});