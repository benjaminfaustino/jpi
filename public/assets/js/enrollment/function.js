(function($) {


    function Backbone() {}


    Backbone.prototype = {
        constract: Backbone,

        mobileMenuClick: function() {

        },

    }

   
    window.Backbone = Backbone;


}(jQuery));


var backbone = new Backbone();

$(window).on("load", function() {
    $(document).on('click', '.mobile-menu', backbone.mobileMenuClick);

    $('#birthday').datepicker({
        format: 'mm/dd/yyyy',
    });

    $('.ui-datepicker-prev span').removeClass();
    $('.ui-datepicker-prev span').addClass("fas fa-arrow-left").html('');

    $('.ui-datepicker-next span').removeClass();
    $('.ui-datepicker-next span').addClass("fas fa-arrow-right").html('');

  $("#fileupload_grades").change(function(){

    imagesPreview(this, 'div.list-grades');

  });

  $("#fileupload_documents").change(function(){

    imagesPreview(this, 'div.list-documents');

  });

  $("#fileupload_image").change(function(){

    imagesPreview(this, 'div.list-image');

  });


    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {

        if (input.files) {
            var filesAmount = input.files.length;

            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(e) {

        $("<span class=\"parent\">" +
              "<img class=\"imageThumb\" src=\"" + e.target.result + "\"/>" +
              "<br/><button class=\"remove btn btn-xs btn-success\">Delete</button>" +
              "</span>").appendTo(placeToInsertImagePreview);

        $(".remove").click(function(){
          $(this).parent(".parent").remove();
        });

                    // $($.parseHTML('<img>')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };
});

$(window).resize(function() {

});

// Vue

// Select 2 component initialization
Vue.component('select2', {
    props: ['options', 'value'],
    template: '<select><slot></slot></select>',
    mounted: function () {
        var vm = this
        $(this.$el)
            // init select2
            .select2({
                placeholder: "Search",
                allowClear: true
            })
            .val(this.value)
            .trigger('change')
            // emit event on change.
            .on('change', function () {

              if (this.value) {

                $(".collection-listing").addClass("hidden");

                $("#" + this.value + ".collection-listing").removeClass("hidden");

              } else {

                $(".collection-listing").removeClass("hidden");                

              }

            });

    },
    methods: {
    },
    watch: {
    },
    destroyed: function () {
        $(this.$el).off().select2('destroy')
    }
});

new Vue({
  el: '#app',
  data: {
      url: '/admin/enrollment-list',
      dataList: [],
      collect: {
        id: "",

        index: "",
      },
      buttonActive: true,
      errors: [],
      formData: [],
      reference: true,
      getValue: null,
      strandDefault: "GAS",
      levelDefault: "year_level_11",
      semesterDefault: "1st_semester",
      strandList: [
        {data: "GAS", display: "GAS"},
        {data: "ABM", display: "ABM"},
        {data: "TVL", display: "TVL"},
        {data: "HUMSS", display: "HUMSS"},
        {data: "STEM", display: "STEM"}
      ],
      grade_level: [
        {data: "year_level_11", display: "Year Level 11"},
        {data: "year_level_12", display: "Year Level 12"}
      ],
      grade_semester: [
        {data: "1st_semester", display: "First Semester"},
        {data: "2nd_semester", display: "Second Semester"}
      ],
      students: [],
      re_render: true,
  },
  created() {

    this.getValue = $("#get-value").val();

    this.fetch();

  },

  mounted() {

    this.sortablefunction();

  },

  computed: {

    collections() {

      this.students = [];
      var self      = this;

      return this.dataList.filter(function(data, index){

        if(self.getValue == "EnrollmentList")
        {
          self.students.push(data);
        }
        else 
        {
          var result = (data.strand == self.strandDefault && data.semester == self.semesterDefault && data.year_level == self.levelDefault) ? true : false;

          if (result) self.students.push(data);
        }

      });

    }
    
  },

  methods: {

    fetch() {

      axios.post(this.url + '/get' + this.getValue)
        .then(response => {

          this.dataList = response.data.data;

          // this.convertDateMoment();

          $("section.content").removeClass("hidden");
          $(".loader").addClass("hidden");

          this.loaderOff();
          
        })
        .catch(error => {
          
        });

    },

    add() {

      this.reset();

      this.buttonActive = true;
    },

    save() {

      axios.post(this.url, this.setFormData(true) )
        .then(response => {

          this.dataList.push(response.data.data);

          this.reset();

          // $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {

          if (error.response.data.errors) this.errors = error.response.data.errors;
          
        });

    },

    edit(item, index) {

      this.collect.id                   = item.id;
      this.collect.enrollment_type      = item.enrollment_type 
      this.collect.first_name           = item.first_name 
      this.collect.middle_name          = item.middle_name 
      this.collect.last_name            = item.last_name 
      this.collect.birth_date           = item.birth_date 
      this.collect.age                  = item.age 
      this.collect.religion             = item.religion 
      this.collect.name_of_parent       = item.name_of_parent 
      this.collect.previous_school      = item.previous_school 
      this.collect.year_level           = item.year_level 
      this.collect.semester             = item.semester 
      this.collect.campus               = item.campus 
      this.collect.gender               = item.gender 
      this.collect.email                = item.email 
      this.collect.phone_number         = item.phone_number 
      this.collect.address              = item.address 
      this.collect.birth_place          = item.birth_place 
      this.collect.civil_status         = item.civil_status 
      this.collect.nationality          = item.nationality 
      this.collect.parent_number        = item.parent_number 
      this.collect.lrn_number           = item.lrn_number 
      this.collect.question             = item.question 
      this.collect.strand               = item.strand 
      this.collect.documents            = item.documents
      this.collect.dates                = item.dates 
      this.collect.index                = index;

      this.buttonActive = false;

      // $("#image-profile").val("");

      this.reference = this.generateDummyId();

      this.formatEnrollDates();

    },

    update() {

      axios.post(this.url + '/put/' + this.collect.id, this.setFormData(false) )
        .then(response => {

          this.updateData(response);

          // $("#image-profile").val("");

          $("#modal-app").modal('hide');

          this.reference = this.generateDummyId();
          
        })
        .catch(error => {
          
      });

    },

    remove(item, index) {

      if(confirm("Are you sure?")) {

        axios.delete(this.url + '/' + item.id)
          .then(response => {

            this.dataList.splice(index, 1);

            this.reference = this.generateDummyId();
            
          })
          .catch(error => {
            
        });

      }

    },

    updateData(response) {

      this.dataList[this.collect.index].id                   = response.data.data.id;
      this.dataList[this.collect.index].enrollment_type      = response.data.data.enrollment_type 
      this.dataList[this.collect.index].first_name           = response.data.data.first_name 
      this.dataList[this.collect.index].middle_name          = response.data.data.middle_name 
      this.dataList[this.collect.index].last_name            = response.data.data.last_name 
      this.dataList[this.collect.index].birth_date           = response.data.data.birth_date 
      this.dataList[this.collect.index].age                  = response.data.data.age 
      this.dataList[this.collect.index].religion             = response.data.data.religion 
      this.dataList[this.collect.index].name_of_parent       = response.data.data.name_of_parent 
      this.dataList[this.collect.index].previous_school      = response.data.data.previous_school 
      this.dataList[this.collect.index].year_level           = response.data.data.year_level 
      this.dataList[this.collect.index].semester             = response.data.data.semester 
      this.dataList[this.collect.index].campus               = response.data.data.campus 
      this.dataList[this.collect.index].gender               = response.data.data.gender 
      this.dataList[this.collect.index].email                = response.data.data.email 
      this.dataList[this.collect.index].phone_number         = response.data.data.phone_number 
      this.dataList[this.collect.index].address              = response.data.data.address 
      this.dataList[this.collect.index].birth_place          = response.data.data.birth_place 
      this.dataList[this.collect.index].civil_status         = response.data.data.civil_status 
      this.dataList[this.collect.index].nationality          = response.data.data.nationality 
      this.dataList[this.collect.index].parent_number        = response.data.data.parent_number 
      this.dataList[this.collect.index].lrn_number           = response.data.data.lrn_number 
      this.dataList[this.collect.index].question             = response.data.data.question 
      this.dataList[this.collect.index].strand               = response.data.data.strand 
      this.dataList[this.collect.index].index                = response.data.data.index;

      this.convertDateMoment();
    },

    reset() {

      this.collect = {
        id: "",
        enrollment_type: "",
        first_name: "",
        middle_name: "",
        last_name: "",
        birth_date: "",
        age: "",
        religion: "",
        name_of_parent: "",
        previous_school: "",
        year_level: "",
        semester: "",
        campus: "",
        gender: "",
        email: "",
        phone_number: "",
        address: "",
        birth_place: "",
        civil_status: "",
        nationality: "",
        parent_number: "",
        lrn_number: "",
        question: "",
        strand: "",
        index: "",
      }

    },

    setFormData(boolean) {

      var grades    = document.querySelector('#fileupload_grades');
      var image     = document.querySelector('#image');
      var documets  = document.querySelector('#documents');

      this.formData = [];

      this.formData = new FormData();
      this.formData.append('id', this.collect.id);
      // this.formData.append('grades', grades.files);
      // this.formData.append('image', image.files);
      // this.formData.append('documets', documets.files);
      this.formData.append('enrollment_type', this.collect.enrollment_type);
      this.formData.append('first_name',  this.collect.first_name);
      this.formData.append('middle_name', this.collect.middle_name);
      this.formData.append('last_name', this.collect.last_name);
      this.formData.append('birth_date',  this.collect.birth_date);
      this.formData.append('age', this.collect.age);
      this.formData.append('religion',  this.collect.religion);
      this.formData.append('name_of_parent',  this.collect.name_of_parent);
      this.formData.append('previous_school', this.collect.previous_school);
      this.formData.append('year_level',  this.collect.year_level);
      this.formData.append('semester',  this.collect.semester);
      this.formData.append('campus',  this.collect.campus);
      this.formData.append('gender',  this.collect.gender);
      this.formData.append('email', this.collect.email);
      this.formData.append('phone_number',  this.collect.phone_number);
      this.formData.append('address', this.collect.address);
      this.formData.append('birth_place', this.collect.birth_place);
      this.formData.append('civil_status',  this.collect.civil_status);
      this.formData.append('nationality', this.collect.nationality);
      this.formData.append('parent_number', this.collect.parent_number);
      this.formData.append('lrn_number',  this.collect.lrn_number);
      this.formData.append('question',  this.collect.question);
      this.formData.append('strand',  this.collect.strand);
      this.formData.append('index', this.collect.index);

      return this.formData;

    },

    generateDummyId() {    

      return Math.floor((Math.random() * 1000) + 1);

    },

    orderBy: function (arr) {
        // Set slice() to avoid to generate an infinite loop!
        return arr.slice().sort(function (a, b) {
              return a.sort - b.sort;
        });
    },

    sortablefunction() {

      var self = this;

      $( "#sortable" ).sortable({
        placeholder: "ui-state-highlight",
        axis: 'y',
        update: function( ) {

            var data = [];

            $($(this).find('.collection-listing')).each(function(index) {
                
                $ID = $(this).attr("id");

                self.dataList.filter(function(collect){

                  if($ID == collect.id) {

                    collect.sort = index;

                    data.push({
                      'id': collect.id,
                      'sort': collect.sort
                    });

                  }

                });

            });

            axios.post(self.url + '/bulk', data)
              .then(response => {
                
              })
              .catch(error => {
                
            });

        }
      });

    },

    convertDateMoment() {

      this.dataList.filter(function(collect){

        // Vue.set(collect, 'birth_date', moment(collect.birthday).format('LL'));
        // Vue.set(collect, 'spiritual_date', moment(collect.spiritual_birthday).format('LL'));
        // Vue.set(collect, 'wedding_date', moment(collect.wedding_anniversary).format('LL'));

        // Vue.set(collect, 'first_name', collect.first_name.toLowerCase());
        // Vue.set(collect, 'last_name', collect.last_name.toLowerCase());
        // Vue.set(collect, 'middle_name', collect.middle_name.toLowerCase());

        collect.first_name  = collect.first_name.toLowerCase();
        collect.last_name   = collect.last_name.toLowerCase();
        collect.middle_name = collect.middle_name.toLowerCase();

      });

    },

    loaderOff() {

      $(".loading").addClass("hidden");

    },

    loaderOn() {

      $(".loading").removeClass("hidden");

    },

    updatePrintStatus(item, index) {

      axios.get('/admin/update-print-status/' + item.id )
        .then(response => {

            this.dataList.filter(function(collect, index){

              if(item.id == collect.id) collect.print_status = response.data.print_status;

            });
          
        })
        .catch(error => {
          
      });

    },

    formatEnrollDates(){

      var self = this;

      this.collect.dates.filter(function(d, ind){

        self.collect.dates[ind].created_at = moment(d.created_at).format("MMM/D/YYYY");

      });
    },

    dateEnrolled(date){

      if (confirm("Are you sure to delete this date?")) {

        axios.post('/admin/delete-enrollment-date/' + date.id)
          .then(response => {

            $("#"+date.id+".date_enrolled_li").remove();
            
          })
          .catch(error => {
            
          });

      }

    },

  }
});
